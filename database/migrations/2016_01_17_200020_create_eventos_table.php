<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->date('dia');
            $table->dateTime('hora_inicio');
            $table->dateTime('hora_fim');
            $table->integer('lugar_id');
            $table->string('lugar_outro');
            $table->integer('tipo_evento_id');
            $table->string('tipo_evento_outro');
            $table->string('tipo_necessidade');
            $table->text('necessidade_extra');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::drop('eventos');
    }
}
