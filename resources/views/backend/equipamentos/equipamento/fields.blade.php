<!-- Tipo Equipamento Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_equipamento_id', 'Tipo Equipamento Id:') !!}
    {!! Form::select('tipo_equipamento_id', $tipos, null, ['class' => 'form-control']) !!}
</div>

<!-- Modelo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('modelo', 'Modelo:') !!}
    {!! Form::text('modelo', null, ['class' => 'form-control']) !!}
</div>

<!-- Obs Field -->
<div class="form-group col-sm-6">
    {!! Form::label('obs', 'Obs:') !!}
    {!! Form::text('obs', null, ['class' => 'form-control']) !!}
</div>



<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.equipamentos.equipamento.index') !!}" class="btn btn-default">Voltar</a>
</div>
