<?php

namespace App\Listeners\Backend\Evento;

use App\Events\Backend\Evento\EventoAtribuido;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Emails\Evento\EventoEmail;

class EventoAtribuidoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(EventoEmail $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  EventoAtribuido  $event
     * @return void
     */
    public function handle(EventoAtribuido $event)
    {
        $evento = $event->evento;
        $users = $event->users;
        $this->mailer->sendEventoAtribuidoEmail($evento, $users);
        $this->mailer->sendEventoAtribuidoEmailOwner($evento, $users);


    }
}
