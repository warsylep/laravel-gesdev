<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $equipamento->id !!}</p>
</div>

<!-- Tipo Equipamento Id Field -->
<div class="form-group">
    {!! Form::label('tipo_equipamento_id', 'Tipo Equipamento Id:') !!}
    <p>{!! $equipamento->tipo_equipamento_id !!}</p>
</div>

<!-- Modelo Field -->
<div class="form-group">
    {!! Form::label('modelo', 'Modelo:') !!}
    <p>{!! $equipamento->modelo !!}</p>
</div>

<!-- Obs Field -->
<div class="form-group">
    {!! Form::label('obs', 'Obs:') !!}
    <p>{!! $equipamento->obs !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $equipamento->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $equipamento->updated_at !!}</p>
</div>

