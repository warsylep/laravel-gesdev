@extends('backend.layouts.master')

@section('page-header')
    <h1>
        {!! app_name() !!}
        <small>{{ trans('strings.backend.dashboard.title') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('strings.backend.dashboard.welcome') }} {!! access()->user()->name !!}!</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div><!-- /.box tools -->
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="col-lg-4">
                <!-- Apply any bg-* class to to the info-box to color it -->
                <div class="info-box bg-green">
                    <a href="{!! route('admin.eventos.evento.index') !!}" style="text-decorations:none; color:inherit;">
                    <span class="info-box-icon"><i class="fa fa-tasks"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Eventos</span>
                        <span class="info-box-number">{!! $eventos->total() !!}</span>
                        <!-- The progress section is optional -->
                        <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                        </div>
                     <span class="progress-description">

                    </span>
                    </div><!-- /.info-box-content -->
                    </a>
                </div><!-- /.info-box -->
            </div>
            <div class="col-lg-4">
                <!-- Apply any bg-* class to to the info-box to color it -->
                <div class="info-box bg-red">
                    <a href="{!! route('admin.eventos.calendario') !!}" style="text-decorations:none; color:inherit;">
                    <span class="info-box-icon"><i class="fa fa-tasks"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Calendário</span>
                        <span class="info-box-number"><br></span>
                        <!-- The progress section is optional -->
                        <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                        </div>
                         <span class="progress-description">

                 </span>
                    </div><!-- /.info-box-content -->
                        </a>
                </div><!-- /.info-box -->
            </div>
            <div class="col-lg-4">
                <!-- Apply any bg-* class to to the info-box to color it -->
                <div class="info-box bg-yellow">
                    <a href="{!! route('admin.eventos.evento.poratribuir') !!}" style="text-decorations:none; color:inherit;">
                    <span class="info-box-icon"><i class="fa fa-tasks"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">Eventos por atribuir</span>
                        <span class="info-box-number">{!! $atr !!}</span>
                        <!-- The progress section is optional -->
                        <div class="progress">
                            <div class="progress-bar" style="width: 100%"></div>
                        </div>
    <span class="progress-description">

    </span>
                    </div><!-- /.info-box-content -->
                        </a>
            </div>
        </div><!-- /.box-body -->
    </div><!--box box-success-->
@endsection