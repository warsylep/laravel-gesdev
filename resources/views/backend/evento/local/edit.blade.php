@extends('backend.layouts.master')

@section('content')
   <section class="content-header">
           <h1>
               Local - Editar
           </h1>
   </section>
   <div class="content">

       <div class="box box-primary">

           <div class="box-body">
               <div class="row">
                   {!! Form::model($lugar, ['route' => ['admin.eventos.local.update', $lugar->id], 'method' => 'patch']) !!}

                    @include('backend.evento.local.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection