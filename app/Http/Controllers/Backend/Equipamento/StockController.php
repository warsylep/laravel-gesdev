<?php

namespace App\Http\Controllers\Backend\Equipamento;

use App\DataTables\StockDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateStockRequest;
use App\Http\Requests\UpdateStockRequest;
use App\Repositories\StockRepository;
use App\Repositories\EquipamentoRepository;
use App\Repositories\ArmazemRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class StockController extends AppBaseController
{
    /** @var  StockRepository */
    private $stockRepository;

    public function __construct(StockRepository $stockRepo, EquipamentoRepository $equipRepo, ArmazemRepository $armaRepo)
    {
        $this->stockRepository = $stockRepo;
        $this->equipRepository = $equipRepo;
        $this->armaRepository = $armaRepo;
    }

    /**
     * Display a listing of the Stock.
     *
     * @param StockDataTable $stockDataTable
     * @return Response
     */
    public function index(StockDataTable $stockDataTable)
    {
        return $stockDataTable->render('backend.equipamentos.stock.index');
    }

    /**
     * Show the form for creating a new Stock.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.equipamentos.stock.create')
            ->withEquip($this->equipRepository->getAllEquipamentos())
            ->withArmaz($this->armaRepository->getAllArmazens());
    }

    /**
     * Store a newly created Stock in storage.
     *
     * @param CreateStockRequest $request
     *
     * @return Response
     */
    public function store(CreateStockRequest $request)
    {
        $input = $request->all();

        $stock = $this->stockRepository->create($input);

        Flash::success('Stock registado com sucesso.');

        return redirect(route('admin.equipamentos.stock.index'));
    }

    /**
     * Display the specified Stock.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $stock = $this->stockRepository->findWithoutFail($id);

        if (empty($stock)) {
            Flash::error('Stock não encontrado');

            return redirect(route('admin.equipamentos.stock.index'));
        }

        return view('backend.equipamentos.stock.show')->with('stock', $stock);
    }

    /**
     * Show the form for editing the specified Stock.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $stock = $this->stockRepository->findWithoutFail($id);

        if (empty($stock)) {
            Flash::error('Stock não encontrado');

            return redirect(route('admin.equipamentos.stock.index'));
        }

        return view('backend.equipamentos.stock.edit')->with('stock', $stock);
    }

    /**
     * Update the specified Stock in storage.
     *
     * @param  int              $id
     * @param UpdateStockRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStockRequest $request)
    {
        $stock = $this->stockRepository->findWithoutFail($id);

        if (empty($stock)) {
            Flash::error('Stock não encontrado');

            return redirect(route('admin.equipamentos.stock.index'));
        }

        $stock = $this->stockRepository->update($request->all(), $id);

        Flash::success('Stock actualizado com sucesso.');

        return redirect(route('admin.equipamentos.stock.index'));
    }

    /**
     * Remove the specified Stock from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $stock = $this->stockRepository->findWithoutFail($id);

        if (empty($stock)) {
            Flash::error('Stock não encontrado');

            return redirect(route('admin.equipamentos.stock.index'));
        }

        $this->stockRepository->delete($id);

        Flash::success('Stock removido com sucesso.');

        return redirect(route('admin.equipamentos.stock.index'));
    }
}
