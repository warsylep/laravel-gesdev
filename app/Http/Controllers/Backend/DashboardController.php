<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Repositories\Backend\Evento\EventoContract;
use App\Models\Evento\Evento;
use Carbon\Carbon;
/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */



class DashboardController extends Controller
{

    public function __construct(
        EventoContract $eventos
    )
    {
        $this->eventos = $eventos;
        Carbon::setLocale('pt');
    }




    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {

        if (access()->hasrole('1')) {
               return view('backend.dashboard')
                ->withEventos($this->eventos->getEventosPaginated(8))
                ->withConfa($this->eventos->getAllEventos()->where('confirmado',0)->count())
                ->withAtr($this->eventos->getAllEventos()->where('atribuido',0)->count());

        }
        return redirect()->route('admin.eventos.evento.meus');
    }

}