<?php
/**
 * Created by PhpStorm.
 * User: nuno.silva
 * Date: 17-01-2016
 * Time: 17:57
 */

namespace App\Http\Controllers\Backend\Evento;

use App\Events\Backend\Evento\NovoEvento;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Evento\EventoContract;
use App\Repositories\Backend\Lugar\LugarContract;
use App\Repositories\Backend\TipoEvento\TipoEventoContract;
use App\Repositories\Backend\User\UserContract;
use Illuminate\Http\Request;
use App\Models\Evento\Evento;
use App\Models\Access\User\User;
use App\Http\Emails\Evento\EventoEmail;
use Carbon\Carbon;
use Event;
use App\Events\Backend\Evento\EventoAtribuido;
use PHPushbullet\PHPushbullet;


/**
 * Class EventoController
 * @package App\Http\Controllers\Backend\Evento
 */
class EventoController extends Controller
{


    /**
     * @param EventoContract $eventos
     */
    public function __construct(
        EventoContract $eventos,
        LugarContract $lugares,
        TipoEventoContract $tipo,
        UserContract $users
    )
    {
      $this->eventos = $eventos;
      $this->lugares = $lugares;
      $this->tipo = $tipo;
      $this->users = $users;
      Carbon::setLocale('pt');

    }


    /**
     *
     */
    public function index()
    {

        if (access()->hasRoles([1,3,5])) {
            return view('backend.evento.evento.index')->withEventos($this->eventos->getAllEventos())
                ->withUsers($this->users->getAllUsers('name', 'asc')->lists('name', 'id'));
        }
        return redirect()->route('admin.eventos.evento.meus');

    }

    /**
     * @return mixed
     */
    public function meusEventos()
    {
        return view('backend.evento.evento.meus')->withEventos($this->eventos->getMeusEventos())
                                                  ->withUsers($this->users->getAllUsers('name','asc')->lists('name','id'));

    }


    public function eventosAttr(){
        return view('backend.evento.evento.atribuidos')->withEventos($this->eventos->getEventosAtribuidos())
                                                    ->withUsers($this->users->getAllUsers('name', 'asc')->lists('name','id'));
    }

    public function PorAtribuir(){
        return view('backend.evento.evento.poratribuir')->withEventos($this->eventos->getEventosPorAtribuir())
                                                    ->withUsers($this->users->getAllUsers('name','asc')->lists('name','id'));
    }


    public function usersAttr($id){
        return $this->eventos->getUsersAttr($id)->lists('name','email','id');
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.evento.evento.create')
            ->withLugares($this->lugares->getAllLugares()->lists('nome','id'))
            ->withTipo($this->tipo->getAllTipoEvento()->lists('tipo_evento','id'));
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request)
    {

        if($this->eventos->create($request)) {
            if (access()->allow('listar-eventos'))
                return redirect()->route('admin.eventos.evento.index')->withFlashSuccess(trans('alerts.backend.eventos.evento.created'));
            return redirect()->route('admin.eventos.evento.meus')->withFlashSuccess(trans('alerts.backend.eventos.evento.created'));
        }else {
            return redirect()->back()->withFlashDanger('Erro ao guardar o evento, contacte o administrador');
        }
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function edit($id, Request $request)
   {
       $evento = $this->eventos->findOrThrowException($id, true);
       $nec = explode(':',$evento->tipo_necessidade);

       return view('backend.evento.evento.edit')
           ->withEvento($evento)
           ->withLugares($this->lugares->getAllLugares()->lists('nome','id'))
           ->withTipo($this->tipo->getAllTipoEvento()->lists('tipo_evento','id'))
           ->withNec($nec);
   }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $temp = $request->input('tipo_necessidade');
        $input['tipo_necessidade'] = implode(':',$temp);
        $this->eventos->update($input,$id);
        return redirect()->route('admin.eventos.evento.index')->withFlashSuccess(trans('alerts.backend.eventos.evento.updated'));
    }

    /**
     * @param $id
     * @param Request $request
     * @return mixed
     */
    public function destroy($id, Request $request)
    {
        $this->eventos->delete($id);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.eventos.evento.deleted'));
    }




    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $evento = $this->eventos->findOrThrowException($id);
        $nec = explode(':',$evento->tipo_necessidade);

        return view('backend.evento.evento.view')
            ->withEvento($evento)
            ->withLugares($this->lugares->getAllLugares()->lists('nome','id'))
            ->withTipo($this->tipo->getAllTipoEvento()->lists('tipo_evento','id'))
            ->withNec($nec);

    }

    /**
     * @return bool
     */
    public function calendario(){
        $events = [];
        $eventos = $this->eventos->getAllEventos();
        foreach($eventos as $evento)
        {
            $events[] = \Calendar::event(
                $evento->tipo_evento->tipo_evento,
                false,
                new \DateTime($evento->dia.$evento->hora_inicio),
                new \DateTime($evento->dia.$evento->hora_fim),
                $evento->id,
                [
                    'description' => $evento->lugar->nome
                ]
            );
        }

        $calendar = \Calendar::addEvents($events) //add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'firstDay' => 1
        ]);
        return view('backend.evento.evento.calendario',['calendar' => $calendar]);
    }


    /**
     * @param $id
     * @param $status
     * @return mixed
     */
    public function conf($id, $status)
    {
        $this->eventos->conf($id, $status);
        return redirect()->back()->withFlashSuccess(trans('alerts.backend.eventos.evento.updated'));
    }

    /**
     * @param Request $request
     * @return int
     */
    public function postAttr(Request $request){
        if($request->ajax()) {

           $evento = Evento::find($request->input('id'));
            foreach ($request->users as $user){
                $users[$user] = $user;

            }
          if($evento->user()->sync($users)){
            event::fire(new EventoAtribuido($evento, $users));
              $this->eventos->atribuir($evento->id);
          }

        }
    }

    public function autoAtribuir($id){
        $evento = Evento::find($id);
        $user = array(access()->id());
        if($evento->user()->sync($user)){
            event::fire(new EventoAtribuido($evento, $user));
            $this->eventos->atribuir($evento->id);
            return redirect()->back()->withFlashSuccess(trans('alerts.backend.eventos.evento.updated'));
        }


    }



}











