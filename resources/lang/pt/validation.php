<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'         => 'O campo :attribute deve ser aceito.',
    'active_url'       => 'O campo :attribute não contém um URL válido.',
    'after'            => 'O campo :attribute deve conter uma data posterior a :date.',
    'alpha'            => 'O campo :attribute deve conter apenas letras.',
    'alpha_dash'       => 'O campo :attribute deve conter apenas letras, números e traços.',
    'alpha_num'        => 'O campo :attribute deve conter apenas letras e números .',
    'array'            => 'O campo :attribute tem que ser um conjunto.',
    'before'           => 'O campo :attribute deve conter uma data anterior a :date.',
    'between'          => [
        'numeric' => 'O campo :attribute deve ter um valor entre :min - :max.',
        'file'    => 'O campo :attribute deve ter um tamanho entre :min - :max kilobytes.',
        'string'  => 'O campo :attribute deve conter entre :min - :max caracteres.',
        'array'   => 'O campo :attribute precisar ter entre :min - :max itens.',
    ],
    'boolean'          => 'O campo :attribute deve ter o valor verdadeiro ou falso.',
    'confirmed'        => 'A confirmação para o campo :attribute não coincide.',
    'date'             => 'O campo :attribute não contém uma data válida.',
    'date_format'      => 'A data indicada para o campo :attribute não respeita o formato :format.',
    'different'        => 'Os campos :attribute e :other devem conter valores diferentes.',
    'digits'           => 'O campo :attribute deve conter :digits dígitos.',
    'digits_between'   => 'O campo :attribute deve conter entre :min a :max dígitos.',
    'email'            => 'O campo :attribute não contém um endereço de email válido.',
    'exists'           => 'O valor selecionado para o campo :attribute é inválido.',
    'filled'           => 'O campo :attribute é obrigatório.',
    'image'            => 'O campo :attribute deve conter uma imagem.',
    'in'               => 'O campo :attribute não contém um valor válido.',
    'integer'          => 'O campo :attribute deve conter um número inteiro.',
    'ip'               => 'O campo :attribute deve conter um IP válido.',
    'json'             => 'O campo :attribute deve conter uma string JSON válida.',
    'max'              => [
        'numeric' => 'O campo :attribute não deve conter um valor superior a :max.',
        'file'    => 'O campo :attribute não deve ter um tamanho superior a :max kilobytes.',
        'string'  => 'O campo :attribute não deve conter mais de :max caracteres.',
        'array'   => 'O campo :attribute deve ter no máximo :max itens.',
    ],
    'mimes'            => 'O campo :attribute deve conter um arquivo do tipo: :values.',
    'min'              => [
        'numeric' => 'O campo :attribute deve ter um valor superior ou igual a :min.',
        'file'    => 'O campo :attribute deve ter no mínimo :min kilobytes.',
        'string'  => 'O campo :attribute deve conter no mínimo :min caracteres.',
        'array'   => 'O campo :attribute deve ter no mínimo :min itens.',
    ],
    'not_in'           => 'O campo :attribute contém um valor inválido.',
    'numeric'          => 'O campo :attribute deve conter um valor numérico.',
    'regex'            => 'O formato do valor para o campo :attribute é inválido.',
    'required'         => 'O campo :attribute é obrigatório.',
    'required_if'      => 'O campo :attribute é obrigatório quando o valor do campo :other é igual a :value.',
    'required_with'    => 'O campo :attribute é obrigatório quando :values está presente.',
    'required_with_all' => 'O campo :attribute é obrigatório quando um dos :values está presente.',
    'required_without' => 'O campo :attribute é obrigatório quanto :values não está presente.',
    'required_without_all' => 'O campo :attribute é obrigatório quando nenhum dos :values está presente.',
    'same'             => 'Os campos :attribute e :other devem conter valores iguais.',
    'size'             => [
        'numeric' => 'O campo :attribute deve conter o valor :size.',
        'file'    => 'O campo :attribute deve ter o tamanho de :size kilobytes.',
        'string'  => 'O campo :attribute deve conter :size caracteres.',
        'array'   => 'O campo :attribute deve ter :size itens.',
    ],
    'string'           => 'O campo :attribute deve ser uma string.',
    'timezone'         => 'O campo :attribute deve ter um fuso horário válido.',
    'unique'           => 'O valor indicado para o campo :attribute já se encontra utilizado.',
    'url'              => 'O formato da URL indicada para o campo :attribute é inválido.',


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention 'attribute.rule' to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'mensagem-customizada',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of 'email'. This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [

        'backend' => [
            'eventos' => [
              'evento' => [
                  'titulo' => 'Titulo',
                'dia'  => 'Dia: ',
                'hora_inicio' => 'Hora de inicio: ',
                'hora_fim' => 'Hora de fim: ',
                'lugar' => 'Localização: ',
                'tipo' => 'Tipo de evento: ',
                'lugar_outro' => 'Outro: ',
                'tipo_outro' => 'Outro tipo: ',
                'tipo_necessidade' => 'Tipo de necessidade: ',
              ],
            ],
            'access' => [
                'permissions' => [
                    'associated_roles' => 'Perfis Associados',
                    'dependencies' => 'Dependências',
                    'display_name' => 'Nome a apresentar',
                    'group' => 'Grupo',
                    'group_sort' => 'Ordenar no Grupo',

                    'groups' => [
                        'name' => 'Nome do Grupo',
                    ],

                    'name' => 'Nome',
                    'system' => 'Sistema?',
                ],

                'roles' => [
                    'associated_permissions' => 'Permissões Associadas',
                    'name' => 'Nome',
                    'sort' => 'Ordenar',
                ],

                'users' => [
                    'active' => 'Ativo',
                    'associated_roles' => 'Perfis Associados',
                    'confirmed' => 'Confirmado',
                    'email' => 'endereço de e-mail',
                    'name' => 'Nome',
                    'other_permissions' => 'Outras permissões',
                    'password' => 'Password',
                    'password_confirmation' => 'Confirme a password',
                    'send_confirmation_email' => 'Enviar E-mail de confirmação',
                ],
            ],
        ],

        'frontend' => [
            'email' => 'endereço de e-mail',
            'name' => 'Nome',
            'password' => 'Password',
            'password_confirmation' => 'Confirme sua senha',
            'old_password' => 'Senha antiga',
            'new_password' => 'Nova Senha',
            'new_password_confirmation' => 'Confirme sua nova senha',
        ],
    ],

];
