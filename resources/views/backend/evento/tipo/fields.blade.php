<!-- Tipo Evento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tipo_evento', 'Tipo Evento:') !!}
    {!! Form::text('tipo_evento', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.eventos.tipo.index') !!}" class="btn btn-default">Cancelar</a>
</div>
