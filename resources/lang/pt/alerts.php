<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain alert messages for various scenarios
    | during CRUD operations. You are free to modify these language lines
    | according to your application's requirements.
    |
    */

    'backend' => [
        'eventos' => [
          'evento' => [
            'created' => 'Evento criado com sucesso',
            'updated' => 'Evento actualizado com sucesso',
             'deleted' => 'Evento apagado com sucesso',
          ],
        ],
        'permissions' => [
            'created' => 'Permissão criada com sucesso.',
            'deleted' => 'Permissão apagada com sucesso.',

            'groups'  => [
                'created' => 'Grupo de permissões criado com sucesso.',
                'updated' => 'Grupo de permissões atualizado com sucesso.',
                'deleted' => 'Grupo de permissões apagado com sucesso.',
            ],

            'updated' => 'Permissão atualizada com sucesso.',
        ],

        'roles' => [
            'created' => 'O perfil foi criado com sucesso.',
            'updated' => 'O perfil foi atualizado com sucesso.',
            'deleted' => 'O perfil foi apagado com sucesso.',
        ],

        'users' => [
            'confirmation_email'  => 'Será enviado um novo email de confirmação.',
            'created'             => 'O utilizador foi criado com sucesso.',
            'deleted'             => 'O utilizador foi apagado com sucesso.',
            'deleted_permanently' => 'O utilizador foi apagado permanentemente.',
            'restored'            => 'O utilizador foi restaurado com sucesso.',
            'updated'             => 'O utilizador foi atualizado com sucesso.',
            'updated_password'    => "A password do utilizador foi alterada com sucesso.",
        ]
    ],
];