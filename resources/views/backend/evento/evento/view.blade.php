@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.eventos.evento.gestao') . ' | ' . trans('labels.backend.eventos.evento.ver'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.eventos.evento.gestao') }}
        <small>{{ trans('labels.backend.eventos.evento.ver') }}</small>
    </h1>
@endsection

@section('content')
    {!! Form::open(['class' => 'form-horizontal', 'role' => 'form', 'method' => '']) !!}

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.eventos.evento.ver') }}</h3>

            <div class="box-tools pull-right">

            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('titulo', trans('validation.attributes.backend.eventos.evento.titulo'), ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-5">
                    {!! $evento->titulo !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('dia', trans('validation.attributes.backend.eventos.evento.dia'), ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-5">
                    {!! $evento->dia !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('hora_inicio', trans('validation.attributes.backend.eventos.evento.hora_inicio'), ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-10">
                    {!! $evento->hora_inicio !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('hora_fim', trans('validation.attributes.backend.eventos.evento.hora_fim'), ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-10">
                    {!! $evento->hora_fim !!}
                </div>
            </div>
            <!--form control-->

            <div class="form-group">
                {!! Form::label('lugar_id', trans('validation.attributes.backend.eventos.evento.lugar'), ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-10">
                    {!! $evento->lugar->nome !!}
                </div>

            </div><!--form control-->
            @if($evento->lugar_outro != '')
            <div class="form-group">
                {!! Form::label('lugar_outro', trans('validation.attributes.backend.eventos.evento.lugar_outro'), ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-10">
                    {!! $evento->lugar_outro !!}
                </div>
            </div>
            @endif

            <div class="form-group">
                {!! Form::label('tipo_evento_id', trans('validation.attributes.backend.eventos.evento.tipo'), ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-10">
                    {!! $evento->tipo_evento->tipo_evento !!}
                </div>
            </div>
            @if($evento->tipo_evento_outro != '')
            <div class="form-group">
                {!! Form::label('tipo_evento_outro', trans('validation.attributes.backend.eventos.evento.tipo_outro'), ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-10">
                    {!! $evento->tipo_evento_outro !!}
                </div>
            </div>
            @endif
            <?php
            $options_check = [
                    'iluminacao_palco'     => 'Iluminação de Palco',
                    'trazem_equipamento'   => 'Trazem equipamento',
                    'som_exterior'         => 'Som Exterior',
                    'som_interior'         => 'Som Interior',
                    'proj'                 => 'Projecção de filmes/fotos',
                    'som_ambiente'         => 'Som Ambiente',
                    'pulpito'              => 'Púlpito',
                    'mesas_cadeiras'       => 'Mesas e cadeiras',
                    'microfones'           => 'Microfones'
            ];
            ?>

            <div class="form-group">
                {!! Form::label('tipo_necessidade', trans('validation.attributes.backend.eventos.evento.tipo_necessidade'), ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-10">
                  @foreach($options_check as $option => $label)
                      @if(in_array($option,$nec))
                          {!! $label !!}<br>
                      @endif
                      @endforeach
                </div>
            </div>


            <div class="form-group">
                {!! Form::label('necessidade_extra', 'Necessidade extra', ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-10">
                     {!! $evento->necessidade_extra !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('created_at', 'Criado em', ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-10">
                    {!! $evento->created_at !!} - {!! $evento->created_at->diffForHumans() !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('updated_at', 'Actualizado em', ['class' => 'col-lg-2 ver-label']) !!}
                <div class="col-lg-10">
                    {!! $evento->updated_at !!} - {!! $evento->updated_at->diffForHumans() !!}
                </div>
            </div>



    </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                <a href="{{URL::previous()}}" class="btn btn-danger btn-xs">{{ trans('buttons.general.voltar') }}</a>
            </div>

            <div class="pull-right">

            </div>
            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->

    {!! Form::close() !!}
@stop

@section('after-scripts-end')
    {!! Html::script('js/backend/eventos/custom.js') !!}

@stop