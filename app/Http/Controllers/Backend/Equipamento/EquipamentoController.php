<?php

namespace App\Http\Controllers\Backend\Equipamento;

use App\DataTables\EquipamentoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateEquipamentoRequest;
use App\Http\Requests\UpdateEquipamentoRequest;
use App\Models\Equipamento;
use App\Models\TipoEquipamento;
use App\Repositories\EquipamentoRepository;
use App\Repositories\TipoEquipamentoRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class EquipamentoController extends AppBaseController
{
    /** @var  EquipamentoRepository */
    private $equipamentoRepository;

    public function __construct(EquipamentoRepository $equipamentoRepo, TipoEquipamentoRepository $tipoEquipamentoRepo)
    {
        $this->equipamentoRepository = $equipamentoRepo;
        $this->tipoEquipamentoRepository = $tipoEquipamentoRepo;
    }

    /**
     * Display a listing of the Equipamento.
     *
     * @param EquipamentoDataTable $equipamentoDataTable
     * @return Response
     */
    public function index(EquipamentoDataTable $equipamentoDataTable)
    {
        return $equipamentoDataTable->render('backend.equipamentos.equipamento.index');
    }

    /**
     * Show the form for creating a new Equipamento.
     *
     * @return Response
     */
    public function create()
    {

        return view('backend.equipamentos.equipamento.create')->withTipos($this->tipoEquipamentoRepository->getAllTipos()->pluck('nome','id'));
    }

    /**
     * Store a newly created Equipamento in storage.
     *
     * @param CreateEquipamentoRequest $request
     *
     * @return Response
     */
    public function store(CreateEquipamentoRequest $request)
    {
        $input = $request->all();

        $equipamento = $this->equipamentoRepository->create($input);

        Flash::success('Equipamento guardado com sucesso.');

        return redirect(route('admin.equipamentos.equipamento.index'));
    }

    /**
     * Display the specified Equipamento.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $equipamento = $this->equipamentoRepository->findWithoutFail($id);

        if (empty($equipamento)) {
            Flash::error('Equipamento não encontrado.');

            return redirect(route('admin.equipamentos.equipamento.index'));
        }

        return view('backend.equipamentos.equipamento.show')->with('equipamento', $equipamento);
    }

    /**
     * Show the form for editing the specified Equipamento.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $equipamento = $this->equipamentoRepository->findWithoutFail($id);

        if (empty($equipamento)) {
            Flash::error('Equipamento não encontrado');

            return redirect(route('admin.equipamentos.equipamento.index'));
        }

        return view('backend.equipamentos.equipamento.edit')->with('equipamento', $equipamento)->withTipos($this->tipoEquipamentoRepository->getAllTipos()->pluck('nome','id'));;
    }

    /**
     * Update the specified Equipamento in storage.
     *
     * @param  int              $id
     * @param UpdateEquipamentoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEquipamentoRequest $request)
    {
        $equipamento = $this->equipamentoRepository->findWithoutFail($id);

        if (empty($equipamento)) {
            Flash::error('Equipamento não encontrado.');

            return redirect(route('admin.equipamentos.equipamento.index'));
        }

        $equipamento = $this->equipamentoRepository->update($request->all(), $id);

        Flash::success('Equipamento actualizado com sucesso.');

        return redirect(route('admin.equipamentos.equipamento.index'));
    }

    /**
     * Remove the specified Equipamento from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $equipamento = $this->equipamentoRepository->findWithoutFail($id);

        if (empty($equipamento)) {
            Flash::error('Equipamento não encontrado.');

            return redirect(route('admin.equipamentos.equipamento.index'));
        }

        $this->equipamentoRepository->delete($id);

        Flash::success('Equipamento eliminado com sucesso.');

        return redirect(route('admin.equipamentos.equipamento.index'));
    }
}
