<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Equipamento
 * @package App\Models
 */
class Equipamento extends Model
{

    public $table = 'equipamentos';
    


    public $fillable = [
        'tipo_equipamento_id',
        'modelo',
        'obs',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'tipo_equipamento_id' => 'integer',
        'obs' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tipo_equipamento_id' => 'required|unique_with:equipamentos,modelo',
        'modelo' => 'required'
    ];


    public function tipos(){
        return $this->belongsTo('App\Models\TipoEquipamento', 'tipo_equipamento_id');
    }
}
