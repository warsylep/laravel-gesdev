<?php


namespace App\Repositories\Backend\Evento;


/**
 * Interface EventoContract
 * @package App\Repositories\Backend\Evento
 */
/**
 * Interface EventoContract
 * @package App\Repositories\Backend\Evento
 */
interface EventoContract
{

    /**
     * @param $id
     * @return mixed
     */
    public function findOrThrowException($id);


    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAllEventos($order_by = 'dia', $sort = 'asc');


    /**
     * @return mixed
     */
    public function getMeusEventos();

    /**
     * @param $per_page
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getEventosPaginated($per_page, $order_by = 'dia', $sort = 'asc');




    /**
     * @param $input
     * @return mixed
     */
    public function create($input);

    /**
     * @param $id
     * @param $input
     * @return mixed
     */
    public function update($id, $input);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);

    /**
     * @param $id
     * @param $status
     * @return mixed
     */
    public function conf($id, $status);

    /**
     * @param $id
     * @return mixed
     */
    public function atribuir($id);

    public function getEventosAtribuidos();

    public function getUsersAttr($id);

    public function getEventosPorAtribuir();




}