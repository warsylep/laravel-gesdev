<?php

namespace App\Listeners\Backend\Evento;

use App\Events\Backend\Evento\EventoConfirmado;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventoConfirmadoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventoConfirmado  $event
     * @return void
     */
    public function handle(EventoConfirmado $event)
    {
        //
    }
}
