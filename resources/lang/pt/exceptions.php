<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Exception Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in Exceptions thrown throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'permissions' => [
                'create_error' => 'Houve um problema ao criar essa permissão. Por favor, tente novamente.',
                'delete_error' => 'Houve um problema ao apagar essa permissão . Por favor, tente novamente.',

                'groups' => [
                    'associated_permissions' => 'Não é possivel apagar esse grupo, existem permissões associadas.',
                    'has_children' => 'Não é possivel apagar esse grupo, existe um grupo filho.',
                    'name_taken' => 'Já existe um grupo com esse nome.',
                ],

                'not_found' => 'Essa permissão não existe.',
                'system_delete_error' => 'Não é possivel apagar uma permissão de sistema',
                'update_error' => 'Houve um problema ao atualizar essa permissão. Por favor, tente novamente.',
            ],

            'roles' => [
                'already_exists' => 'Esse perfil já existe. Por favor, escolha um nome diferente.',
                'cant_delete_admin' => 'Não é possivel apagar o perfil de Administrador.',
                'create_error' => 'Houve um problema ao criar esse perfil. Por favor, tente novamente.',
                'delete_error' => 'Houve um problema ao apagar esse perfil. Por favor, tente novamente.',
                'has_users' => 'Não é possivel apagar um perfil com utilizadores associados..',
                'needs_permission' => 'Tem que selecionar pelo menos uma permissão para este perfil.',
                'not_found' => 'Este perfil não existe.',
                'update_error' => 'Houve um problema ao atualizar esse perfil. Por favor, tente novamente.',
            ],

            'users' => [
                'cant_deactivate_self' => 'Não pode desactivar o seu próprio utilizador.',
                'cant_delete_self' => 'Não pode apagar o seu próprio utilizador.',
                'create_error' => 'Houve um problema ao criar esse utilizador. Por favor, tente novamente.',
                'delete_error' => 'Houve um problema ao excluir esse utilizador. Por favor, tente novamente.',
                'email_error' => 'Esse endereço de e-mail pertence a um utilizador diferente.',
                'mark_error' => 'Houve um problema ao atualizar esse utilizador. Por favor, tente novamente',
                'not_found' => 'Esse utilizador não existe.',
                'restore_error' => 'Houve um problema ao restaurar esse utilizador. Por favor, tente novamente.',
                'role_needed_create' => 'Tem que seleccionar pelo menos 1 perfil. O utilizador foi criado, mas desativado.',
                'role_needed' => 'Tem que seleccionar pelo menos 1 perfil.',
                'update_error' => 'Houve um problema ao atualizar esse utilizador. Por favor, tente novamente.',
                'update_password_error' => 'Houve um problema ao alterar a password do utilizador. Por favor, tente novamente.',
            ],
        ],
    ],

    'frontend' => [
        'auth' => [
            'confirmation' => [
                'already_confirmed' => 'A conta já está confirmada.',
                'confirm' => 'Confirme a sua conta!',
                'created_confirm' => 'A sua conta foi criada com sucesso. Enviamos um e-mail para confirmar a sua conta.',
                'mismatch' => 'O código de confirmação não corresponde aos nossos registos.',
                'not_found' => 'O código de confirmação não existe.',
                'resend' => 'A conta não está confirmada. Por favor, clique no link de confirmação no seu e-mail, ou <a href="' . route('account.confirm.resend', ':token') . '">clique aqui</a> para reenviar o e-mail de confirmação.',
                'success' => 'A conta foi confirmada com sucesso!',
                'resent' => 'Foi enviado um novo email de confirmação.',
            ],

            'deactivated' => 'A conta foi desativada.',
            'email_taken' => 'Esse endereço de e-mail já foi utilizado.',

            'password' => [
                'change_mismatch' => 'Essa não é a sua password antiga.',
            ],


        ],
    ],
];