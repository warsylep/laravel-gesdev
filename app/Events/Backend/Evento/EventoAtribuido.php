<?php

namespace App\Events\Backend\Evento;

use App\Models\Evento\Evento;
use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventoAtribuido extends Event
{
    use SerializesModels;
    public $evento;
    public $users;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Evento $evento, $users)
    {
        $this->evento = $evento;
        $this->users = $users;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
