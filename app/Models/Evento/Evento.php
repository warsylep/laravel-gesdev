<?php

namespace App\Models\Evento;

use App\Models\Evento\Traits\Attribute\EventoAttribute;
use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    use EventoAttribute;
    protected $guarded = array('id');

    public function user(){
        return $this->belongsToMany('App\Models\Access\User\User')->withTimestamps();
    }

    public function tipo_evento() {
        return $this->belongsTo('App\Models\Evento\TipoEvento', 'tipo_evento_id');
    }

    public function lugar() {
       return  $this->belongsTo('App\Models\Evento\Lugar','lugar_id');
    }

    public function owner()
    {
        return $this->belongsTo('App\Models\Access\User\User', 'owner_id', 'id');
    }

}


