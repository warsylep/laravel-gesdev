<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'A password deverá conter pelo menos seis carateres e ser igual à confirmação.',
    'reset'    => 'Sua password foi redefinida!',
    'sent'     => 'Foi enviado um link de recuperação de password por e-mail.',
    'token'    => 'O código de recuperação de password é inválido.',
    'user'     => 'Não conseguimos encontrar nenhum utilizador com o endereço de e-mail especificado.',


];
