@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.eventos.evento.gestao'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.eventos.evento.gestao') }}
        <small>{{ trans('labels.backend.eventos.evento.todos') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.eventos.evento.todos') }}</h3>

            <div class="box-tools pull-right">
                {{-- @include('backend.access.includes.partials.header-buttons')--}}
            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            @if(isset($eventos))
            <div class="table-responsive">
                <table class="text-center table table-striped table-bordered table-hover" id="dados">
                    <thead>
                    <tr>
                        <th>{{ trans('labels.backend.eventos.evento.tabela.owner') }}</th>
                        <th>{{ trans('labels.backend.eventos.evento.tabela.titulo') }}</th>
                        <th>{{ trans('labels.backend.eventos.evento.tabela.data') }}</th>
                        <th>{{ trans('labels.backend.eventos.evento.tabela.lugar') }}</th>
                        <th>{{ trans('labels.backend.eventos.evento.tabela.tipo') }}</th>
                        <th>{{ trans('labels.backend.eventos.evento.tabela.atribuido') }}</th>
                        <th>{{ trans('labels.backend.eventos.evento.tabela.confirmado') }}</th>
                        <th>{{ trans('labels.general.actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach ($eventos as $evento)

                        <tr>
                            <td>{!! $evento->owner->name !!}</td>
                            <td>{!! $evento->titulo !!}</td>
                            <td>{!! $evento->dia !!}</td>
                            <td>{!! $evento->lugar->nome !!}</td>
                            <td>{!! $evento->tipo_evento->tipo_evento !!}</td>
                            <td>{!! $evento->atribuido_label !!}</td>
                            <td>{!! $evento->confirmado_label !!}</td>
                            <td>{!! $evento->view_button !!}{!! $evento->edit_button !!}{!! $evento->delete_button !!} {!! $evento->conf_button !!} {!! $evento->attr_button !!} {!! $evento->auto_button !!}</td>
                            @include('backend.evento.evento.all_attr')

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            @else
                <h2>Sem Eventos</h2>
                @endif

            <div class="pull-left">

            </div>

            <div class="pull-right">

            </div>

            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->

{!! Html::script('js/backend/attr.js') !!}

@stop
