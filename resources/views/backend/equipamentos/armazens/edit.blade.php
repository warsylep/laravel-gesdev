@extends('backend.layouts.master')

@section('content')
   <section class="content-header">
           <h1>
               Armazéns - Editar
           </h1>
   </section>
   <div class="content">

       <div class="box box-primary">

           <div class="box-body">
               <div class="row">
                   {!! Form::model($armazem, ['route' => ['admin.equipamentos.armazens.update', $armazem->id], 'method' => 'patch']) !!}

                    @include('backend.equipamentos.armazens.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection