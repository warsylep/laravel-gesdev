<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tipo->id !!}</p>
</div>

<!-- Tipo Evento Field -->
<div class="form-group">
    {!! Form::label('tipo_evento', 'Tipo Evento:') !!}
    <p>{!! $tipo->tipo_evento !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $tipo->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $tipo->updated_at !!}</p>
</div>

