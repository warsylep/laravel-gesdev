@extends('backend.layouts.master')

@section('content')
 <section class="content-header">
        <h1>
            Tipo de evento - Criar
        </h1>
    </section>
    <div class="content">

        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.eventos.tipo.store']) !!}

                              @include('backend.evento.tipo.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
