<?php
/**
 * Created by PhpStorm.
 * User: nuno.silva
 * Date: 17-01-2016
 * Time: 16:48
 */

Route::group([
    'prefix'     => 'equipamentos',
    'namespace'  => 'Equipamento',
        ], function() {

            route::resource('tipo','TipoEquipamentoController');

});



