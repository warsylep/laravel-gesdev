@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.eventos.evento.gestao') . ' | ' . trans('labels.backend.eventos.evento.criar'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.eventos.evento.gestao') }}
        <small>{{ trans('labels.backend.eventos.evento.criar') }}</small>
    </h1>
@endsection

@section('content')
    {!! Form::open(['route' => 'admin.eventos.evento.store', 'class' => 'form-horizontal', 'role' => 'form', 'method' => 'post']) !!}

    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.eventos.evento.criar') }}</h3>

            <div class="box-tools pull-right">

            </div>
        </div><!-- /.box-header -->

        <div class="box-body">
            <div class="form-group">
                {!! Form::label('titulo', trans('validation.attributes.backend.eventos.evento.titulo'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-5">
                    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('dia', trans('validation.attributes.backend.eventos.evento.dia'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-5">
                    {!! Form::date('dia', null, ['class' => 'form-control']) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('hora_inicio', trans('validation.attributes.backend.eventos.evento.hora_inicio'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::time('hora_inicio',null, ['class' => 'form-control']) !!}
                </div>
            </div><!--form control-->

            <div class="form-group">
                {!! Form::label('hora_fim', trans('validation.attributes.backend.eventos.evento.hora_fim'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::time('hora_fim',null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <!--form control-->

            <div class="form-group">
                {!! Form::label('lugar_id', trans('validation.attributes.backend.eventos.evento.lugar'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::select('lugar_id',$lugares, null,['class' => 'form-control']) !!}
                </div>

            </div><!--form control-->
            <div class="form-group">
                {!! Form::label('lugar_outro', trans('validation.attributes.backend.eventos.evento.lugar_outro'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('lugar_outro', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.eventos.evento.lugar_outro')]) !!}
                </div>
            </div>

            <div class="form-group">
                {!! Form::label('tipo_evento_id', trans('validation.attributes.backend.eventos.evento.tipo'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::select('tipo_evento_id',$tipo, null, ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('tipo_evento_outro', trans('validation.attributes.backend.eventos.evento.tipo_outro'), ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::text('tipo_evento_outro', null, ['class' => 'form-control', 'placeholder' => trans('validation.attributes.backend.eventos.evento.tipo_outro')]) !!}
                </div>
            </div>
            <?php
            $options_check = [
                    'iluminacao_palco'     => 'Iluminação de Palco',
                    'trazem_equipamento'   => 'Trazem equipamento',
                    'som_exterior'         => 'Som Exterior',
                    'som_interior'         => 'Som Interior',
                    'proj'                 => 'Projecção de filmes/fotos',
                    'som_ambiente'         => 'Som Ambiente',
                    'pulpito'              => 'Púlpito',
                    'mesas_cadeiras'       => 'Mesas e cadeiras',
                    'microfones'           => 'Microfones'
            ];
            ?>
            <div class="form-group">
               <label class="col-lg-2 control-label">Necessidades</label>
                 @foreach($options_check as $option => $option_label)
                     <div class="col-lg-2"></div>
                    <div class="col-lg-10">
                           <input type="checkbox" value="{{$option}}" name="tipo_necessidade[]" /> - <label for="{{$option}}">{{$option_label}}</label>
                        </div>
                        @endforeach
                </div>
            <div class="form-group">
                {!! Form::label('necessidade_extra', 'Necessidade extra', ['class' => 'col-lg-2 control-label']) !!}
                <div class="col-lg-10">
                    {!! Form::textarea('necessidade_extra',null, ['rows' => '4']) !!}
                </div>
            </div>




        </div><!-- /.box-body -->
    </div><!--box-->

    <div class="box box-info">
        <div class="box-body">
            <div class="pull-left">
                <a href="{{URL::previous()}}" class="btn btn-danger btn-xs">{{ trans('buttons.general.cancel') }}</a>
            </div>

            <div class="pull-right">
                <input type="submit" class="btn btn-success btn-xs" value="{{ trans('buttons.general.crud.create') }}" />
            </div>
            <div class="clearfix"></div>
        </div><!-- /.box-body -->
    </div><!--box-->

    {!! Form::close() !!}
@stop

@section('after-scripts-end')
    {!! Html::script('js/backend/eventos/custom.js') !!}

@stop