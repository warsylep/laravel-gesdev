@extends('backend.layouts.master')

@section('content')
 <section class="content-header">
        <h1>
            Armazéns - Adicionar
        </h1>
    </section>
    <div class="content">

        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.equipamentos.armazens.store']) !!}

                              @include('backend.equipamentos.armazens.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
