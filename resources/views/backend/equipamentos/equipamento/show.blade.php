@extends('backend.layouts.master')

@section('content')
    <section class="content-header">
        <h1>
            Equipamento - Ver
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                  @include('backend.equipamentos.equipamento.show_fields')
                  <a href="{!! route('admin.equipamentos.equipamento.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
