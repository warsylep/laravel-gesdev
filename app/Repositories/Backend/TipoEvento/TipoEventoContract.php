<?php
/**
 * Created by PhpStorm.
 * User: nuno.silva
 * Date: 24-01-2016
 * Time: 23:23
 */

namespace App\Repositories\Backend\TipoEvento;


interface TipoEventoContract
{

    public function findOrThrowException($id);


    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAllTipoEvento($order_by = 'id', $sort = 'asc');

    /**
     * @param $per_page
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getTipoEventoPaginated($per_page, $order_by = 'id', $sort = 'asc');

    /**
     * @param $input
     * @return mixed
     */
    public function create($input);

    /**
     * @param $id
     * @param $input
     * @return mixed
     */
    public function update($id, $input);

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id);


}