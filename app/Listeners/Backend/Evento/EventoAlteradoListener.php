<?php

namespace App\Listeners\Backend\Evento;

use App\Events\Backend\Evento\EventoAlterado;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Emails\Evento\EventoEmail;

class EventoAlteradoListener
{
    /**
     * Create the event listener.
     *
     * @param EventoEmail $mailer
     */
    public function __construct(EventoEmail $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  EventoAlterado  $event
     * @return void
     */
    public function handle(EventoAlterado $event)
    {
        $evento = $event->evento;
    }
}
