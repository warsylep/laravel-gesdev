@extends('backend.layouts.master')

@section('content')
 <section class="content-header">
        <h1>
            Local - Adicionar
        </h1>
    </section>
    <div class="content">

        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.eventos.local.store']) !!}

                              @include('backend.evento.local.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
