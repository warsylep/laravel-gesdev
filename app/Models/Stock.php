<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Stock
 * @package App\Models
 */
class Stock extends Model
{

    public $table = 'stock';
    


    public $fillable = [
        'equipamento_id',
        'armazem_id',
        'stock'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'equipamento_id' => 'integer',
        'armazem_id' => 'integer',
        'stock' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'equipamento_id' => 'required|unique_with:stock,armazem_id',
        'armazem_id' => 'required',
        'stock' => 'required'
    ];
}
