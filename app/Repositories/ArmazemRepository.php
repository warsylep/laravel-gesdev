<?php

namespace App\Repositories;

use App\Models\Armazem;
use InfyOm\Generator\Common\BaseRepository;

class ArmazemRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Armazem::class;
    }


    public function getAllArmazens($order_by = 'id', $sort = 'asc'){
        return Armazem::orderBy($order_by, $sort)->pluck('nome','id');
    }
}
