@extends('backend.layouts.master')

@section('content')
   <section class="content-header">
           <h1>
               Equipamento - Editar
           </h1>
   </section>
   <div class="content">

       <div class="box box-primary">

           <div class="box-body">
               <div class="row">
                   {!! Form::model($equipamento, ['route' => ['admin.equipamentos.equipamento.update', $equipamento->id], 'method' => 'patch']) !!}

                    @include('backend.equipamentos.equipamento.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection