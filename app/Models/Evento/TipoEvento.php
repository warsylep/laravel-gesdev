<?php

namespace App\Models\Evento;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TipoEvento
 * @package App\Models\Evento
 */
class TipoEvento extends Model
{
    protected $table = 'tipos_evento';

    public function eventos()
    {
        return $this->hasMany('App\Models\Evento\Evento', 'tipo_evento_id');
    }

}