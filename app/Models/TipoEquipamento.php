<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class TipoEquipamento
 * @package App\Models
 */
class TipoEquipamento extends Model
{

    public $table = 'tipo_equipamentos';
    


    public $fillable = [
        'nome'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nome' => 'required|max:255|unique:tipo_equipamentos'
    ];


    public function equipamentos(){
        return $this->hasMany('App\Models\Equipamento', 'tipo_equipamento_id');
    }
}
