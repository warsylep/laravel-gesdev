<?php
/**
 * Created by PhpStorm.
 * User: nuno.silva
 * Date: 18-05-2016
 * Time: 17:09
 */

Route::group([
    'prefix'     => 'equipamentos',
    'namespace'  => 'Equipamento',
    'middleware' => 'access.routeNeedsPermission:acesso-equipamentos',
], function() {

    route::resource('equipamento','EquipamentoController');

});