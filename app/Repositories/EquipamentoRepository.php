<?php

namespace App\Repositories;

use App\Models\Equipamento;
use InfyOm\Generator\Common\BaseRepository;
use DB;

class EquipamentoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tipo_equipamento_id',
        'modelo',
        'obs',
        'created_at',
        'updated_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Equipamento::class;
    }
    public function getAllEquipamentos($order_by = 'id', $sort = 'asc'){

       $equip =  Equipamento::query()
            ->select([
                'equipamentos.id',
                'equipamentos.tipo_equipamento_id',
                'equipamentos.modelo',
                'equipamentos.obs',
                'equipamentos.created_at',
                'equipamentos.updated_at',
                'tipo_equipamentos.nome as tipo',
                 DB::raw('CONCAT(tipo_equipamentos.nome, " ", equipamentos.modelo) AS full_name')
            ])->leftJoin('tipo_equipamentos', 'equipamentos.tipo_equipamento_id', '=', 'tipo_equipamentos.id')
           ->pluck('full_name','id');

        return $equip;

    }


}
