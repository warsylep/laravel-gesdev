<?php

namespace App\Http\Controllers\Backend\Evento;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\Lugar\LugarContract;
use App\DataTables\LugarDataTable;
use Illuminate\Http\Request;
Use Flash;

class LugarController extends Controller
{
    public function __construct (LugarContract $lugares)
    {
        $this->lugares = $lugares;
    }

    public function index(LugarDataTable $lugarDataTable)
    {
        return $lugarDataTable->render('backend.evento.local.index');
    }

    public function create()
    {
        return view('backend.evento.local.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $lugar = $this->lugares->create($input);

        Flash::success('Armazém guardado com sucesso.');

        return redirect(route('admin.eventos.local.index'));
    }

    public function show($id)
    {
        $lugar = $this->lugares->findOrThrowException($id);

        if (empty($lugar)) {
            Flash::error('Local não encontrado');

            return redirect(route('admin.eventos.local.index'));
        }

        return view('backend.evento.local.show')->with('lugar', $lugar);
    }

    public function edit($id)
    {
        $lugar = $this->lugares->findOrThrowException($id);

        if (empty($lugar)) {
            Flash::error('Local não encontrado');

            return redirect(route('admin.eventos.local.index'));
        }

        return view('backend.evento.local.edit')->with('lugar', $lugar);
    }

    public function update($id, Request $request)
    {
      $lugar = $this->lugares->findOrThrowException($id);

        if (empty($lugar)) {
            Flash::error('Local não encontrado');

            return redirect(route('admin.eventos.local.index'));
        }

        $lugar = $this->lugares->update($id, $request->all());

        Flash::success('Local actualizado com sucesso.');

        return redirect(route('admin.eventos.local.index'));
    }

    public function destroy($id)
    {
        $lugar = $this->lugares->findOrThrowException($id);

        if (empty($lugar)) {
            Flash::error('Local não encontrado');

            return redirect(route('admin.eventos.local.index'));
        }

        $this->lugares->delete($id);

        Flash::success('Local apagado com sucesso.');

        return redirect(route('admin.eventos.local.index'));
    }
}