@extends('backend.layouts.master')

@section('content')
   <section class="content-header">
           <h1>
               Tipo de evento - Editar
           </h1>
   </section>
   <div class="content">

       <div class="box box-primary">

           <div class="box-body">
               <div class="row">
                   {!! Form::model($tipo, ['route' => ['admin.eventos.tipo.update', $tipo->id], 'method' => 'patch']) !!}

                    @include('backend.evento.tipo.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection