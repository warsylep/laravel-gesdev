<?php

namespace App\Http\Emails\Evento;
use Illuminate\Contracts\Mail\Mailer;
use App\Repositories\Backend\Role\RoleRepositoryContract;
use App\Models\Access\User\User;
use App\Models\Evento\Evento;
use Carbon\Carbon;



/**
 * Created by PhpStorm.
 * User: nuno.silva
 * Date: 27-01-2016
 * Time: 15:45
 */
class EventoEmail

{

    public $roles;
    public function __construct(Mailer $mailer, RoleRepositoryContract $roles)
    {
        $this->mailer = $mailer;
        $this->roles = $roles;
        Carbon::setLocale('pt');
    }

    public function findAdminEmails()
    {

        $role =  $this->roles->findOrThrowException(1);
        $emails = $role->users->pluck('email');
        return array($emails);

    }

    public function findUserEmails($users)
    {
        foreach($users as $user) {
            $email[$user] = User::find($user)->email;
            }
        return $email;
    }

    public function findOwnerEmail($id)
    {
        $evento = Evento::find($id);
        $email = $evento->owner()->first()->email;
        return ($email);
    }


    public function deliver()
    {
        $to = $this->to;
        $subject = $this->subject;
        $this->mailer->queue($this->view, $this->data, function ($message) use ($to, $subject){
            $message->to($to)->subject($subject);
        });
    }


    /**
     * @param $evento
     * Functions to send emails
     */



    public function sendNewEventoEmailAdmins($evento)
    {

        $emails = $this->findAdminEmails();
        $flat = implode(',',array_flatten($emails));
        $this->to = explode(',',$flat);
        $this->view = 'backend.emails.novoevento_admins';
        $this->data = ['evento' => $evento, 'user' => access()->user()->name];
        $this->subject = "Novo evento registado";
        $this->deliver();

    }


    public function sendNewEventoEmailOwner($evento){
        $email = $this->findOwnerEmail($evento->id);
        $this->to = $email;
        $this->view = 'backend.emails.novoevento_owner';
        $this->data = ['evento' => $evento];
        $this->subject = "Evento Registado";
        $this->deliver();
    }


    public function sendEventoAtribuidoEmail($evento, $users)
    {
        $emails = $this->findUserEmails($users);
        $flat = implode(',',array_flatten($emails));
        $this->to = explode(',',$flat);
        $this->view = 'backend.emails.eventoatribuido';
        $this->data = ['evento' => $evento, 'user' => access()->user()->name];
        $this->subject = "Novo evento atribuido";
        $this->deliver();
    }


    public function sendEventoAtribuidoEmailOwner($evento, $users)
    {
        foreach ($users as $user) {
        $tecnicos[$user] = User::find($user);
        }
        $emails = $this->findOwnerEmail($evento->id);
        $this->to = $emails;
        $this->view = 'backend.emails.eventoatribuido_owner';
        $this->data = ['evento' => $evento, 'tecnicos' => $tecnicos];
        $this->subject = "O seu evento foi atribuido.";
        $this->deliver();
    }

}
