<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $stock->id !!}</p>
</div>

<!-- Equipamento Id Field -->
<div class="form-group">
    {!! Form::label('equipamento_id', 'Equipamento Id:') !!}
    <p>{!! $stock->equipamento_id !!}</p>
</div>

<!-- Armazem Id Field -->
<div class="form-group">
    {!! Form::label('armazem_id', 'Armazem Id:') !!}
    <p>{!! $stock->armazem_id !!}</p>
</div>

<!-- Stock Field -->
<div class="form-group">
    {!! Form::label('stock', 'Stock:') !!}
    <p>{!! $stock->stock !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $stock->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $stock->updated_at !!}</p>
</div>

