@extends('backend.layouts.master')

@section('content')
 <section class="content-header">
        <h1>
            Tipos de Equipamento - Adicionar
        </h1>
    </section>
    <div class="content">

        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.equipamentos.tipo.store']) !!}

                              @include('backend.equipamentos.tipo.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
