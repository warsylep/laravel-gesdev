﻿# LARAVEL-GESDEV #

Plataforma de gestão de pedidos de eventos em laravel 5.2.

### What is this repository for? ###

* CRUD de eventos, utilizadores, lugares e tipos de eventos
* Atribuição de eventos a users
* Confirmação de eventos
* Auto-atribuição pelos técnicos
* Notificações via email de novos eventos / eventos atribuidos
* Gestâo de Equipamentos para os eventos (Work in progress)
    * Armazéns
    * Tipos de equipamento
    * Equipamentos
    * Stock
    
### How do I get set up? ###

* set up bd
* set up .env
* composer update
* artisan migrate?
* ??????
* PROFIT!

### Contribution guidelines ###

* Faz codigo
* Envia codigo
* Merge codigo

### Who do I talk to? ###

* Repo owner or admin
* Yo momma!