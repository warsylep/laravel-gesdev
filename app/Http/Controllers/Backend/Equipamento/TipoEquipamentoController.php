<?php

namespace App\Http\Controllers\Backend\Equipamento;

use App\DataTables\TipoEquipamentoDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTipoEquipamentoRequest;
use App\Http\Requests\UpdateTipoEquipamentoRequest;
use App\Repositories\TipoEquipamentoRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;
use Yajra\Datatables\Datatables;

class TipoEquipamentoController extends AppBaseController
{
    /** @var  TipoEquipamentoRepository */
    private $tipoEquipamentoRepository;

    public function __construct(TipoEquipamentoRepository $tipoEquipamentoRepo)
    {
        $this->tipoEquipamentoRepository = $tipoEquipamentoRepo;
    }

    /**
     * Display a listing of the TipoEquipamento.
     *
     * @param TipoEquipamentoDataTable $tipoEquipamentoDataTable
     * @return Response
     */
    public function index(TipoEquipamentoDataTable $tipoEquipamentoDataTable)
    {
        return $tipoEquipamentoDataTable->render('backend.equipamentos.tipo.index');
    }

    /**
     * Show the form for creating a new TipoEquipamento.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.equipamentos.tipo.create');
    }

    /**
     * Store a newly created TipoEquipamento in storage.
     *
     * @param CreateTipoEquipamentoRequest $request
     *
     * @return Response
     */
    public function store(CreateTipoEquipamentoRequest $request)
    {
        $input = $request->all();

        $tipoEquipamento = $this->tipoEquipamentoRepository->create($input);

        Flash::success('Tipo de equipamento guardado com sucesso.');

        return redirect(route('admin.equipamentos.tipo.index'));
    }

    /**
     * Display the specified TipoEquipamento.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $tipoEquipamento = $this->tipoEquipamentoRepository->findWithoutFail($id);

        if (empty($tipoEquipamento)) {
            Flash::error('Registo nao encontrado');

            return redirect(route('admin.equipamentos.tipo.index'));
        }

        return view('backend.equipamentos.tipo.show')->with('tipoEquipamento', $tipoEquipamento);
    }

    /**
     * Show the form for editing the specified TipoEquipamento.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $tipoEquipamento = $this->tipoEquipamentoRepository->findWithoutFail($id);

        if (empty($tipoEquipamento)) {
            Flash::error('Registo não encontrado.');

            return redirect(route('admin.equipamentos.tipo.index'));
        }

        return view('backend.equipamentos.tipo.edit')->with('tipoEquipamento', $tipoEquipamento);
    }

    /**
     * Update the specified TipoEquipamento in storage.
     *
     * @param  int              $id
     * @param UpdateTipoEquipamentoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTipoEquipamentoRequest $request)
    {
        $tipoEquipamento = $this->tipoEquipamentoRepository->findWithoutFail($id);

        if (empty($tipoEquipamento)) {
            Flash::error('Registo não encontrado.');

            return redirect(route('admin.equipamentos.tipo.index'));
        }

        $tipoEquipamento = $this->tipoEquipamentoRepository->update($request->all(), $id);

        Flash::success('Registo actualizado com sucesso.');

        return redirect(route('admin.equipamentos.tipo.index'));
    }

    /**
     * Remove the specified TipoEquipamento from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $tipoEquipamento = $this->tipoEquipamentoRepository->findWithoutFail($id);

        if (empty($tipoEquipamento)) {
            Flash::error('Registo não encontrado.');

            return redirect(route('admin.equipamentos.tipo.index'));
        }

        $this->tipoEquipamentoRepository->delete($id);

        Flash::success('Registo apagado com sucesso.');

        return redirect(route('admin.equipamentos.tipo.index'));
    }
}
