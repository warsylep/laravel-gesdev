@extends('backend.layouts.master')

@section('content')
 <section class="content-header">
        <h1>
            Equipamento - Adicionar
        </h1>
    </section>
    <div class="content">

        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.equipamentos.equipamento.store']) !!}

                              @include('backend.equipamentos.equipamento.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
