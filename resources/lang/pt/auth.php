<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Estas credenciais não correspondem com nossos registos.',
    'general_error' => 'Não tem permissão para essa acção.',
    'socialite' => [
        'unacceptable' => ':provider não é um tipo de login aceitável.',
    ],
   'throttle' => 'Limite de tentativas de login. Tente novamente em :seconds segundos.',
    'unknown' => 'Ocorreu um erro desconhecido.',
];
