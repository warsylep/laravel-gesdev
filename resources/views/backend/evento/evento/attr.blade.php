<div class="modal fade" id="attr{!! $evento->id !!}" data-backdrop="static" >
    <div class="modal-dialog" >
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" ><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Atribuir Evento</h4>
            </div>
            <div class="modal-body" id="modal-body{{$evento->id}}">
                <div class="loader" id="loader{{$evento->id}}" style="display: none">
                    {!! Html::image('img/483.gif') !!}
                </div>
                <div class="erro" id="erro{{$evento->id}}">

                </div>

                {!! Form::open(array('route' => array('admin.eventos.evento.postattr', $evento->id))) !!}
                {!! Form::label('users'.$evento->id, 'Técnicos', ['class' => 'col-lg-2 control-label']) !!}

                {!! Form::select('users'.$evento->id,$users, null,['class' => 'form-control','multiple','required', 'placeholder' => 'Escolha uma ou mais opções']) !!}
                <span id="helpBlock" class="help-block bg-info">A tarefa será atribuida aos utilizadores seleccionados.</span>


            </div>
            <div class="modal-footer" id="modal-footer{{$evento->id}}">
                <button type="button" class="btn btn-default" id="cancel{{$evento->id}}_btn" data-dismiss="modal">Voltar</button>
                <button type="button" class="btn btn-primary" id="attr{{$evento->id}}_btn">Guardar</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<script id="sucesso" type="text/html">
    <div class="alert alert-success alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-check"></i> Sucesso</h4>
    O evento foi atribuido com sucesso aos utilizadores especificados.
    </div>
</script>
<script id="erro" type="text/html">
    <div class="alert alert-danger alert-dismissible">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <h4><i class="icon fa fa-ban"></i> Ocorreu um erro! </h4>
    Tem que escolher pelo menos um utilizador para atribuir o evento!
    </div>
</script>
<script id="erro2" type="text/html">
    <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-ban"></i> Ocorreu um erro! </h4>
        Ocorreu um erro não especificado.
        Contacte o administrador.
    </div>
</script>

<script type="text/javascript">

    $(document).ready(function(){

        $('#la_{{$evento->id}}').click(function(){
            $.get("{{$evento->id}}/usersAttr",function(data) {
                console.log(data);
                var array = [];
                for(i in data) {
                    array.push(data[i]);
                }
                var vals = (array.join(", "));
                $('#la_{{$evento->id}}').popover({
                    title: "Atribuido a:",
                    content: vals

                });
                $('#la_{{$evento->id}}').popover("show");
            });
        });

        $('#attr{{$evento->id}}_btn').click(function(e){
            var $el = $("#modal-body{{$evento->id}}");
            e.preventDefault();
            $el.faLoading();
            if($('#users{{$evento->id}}').val() != null) {
                $.ajax({
                    url: '{{$evento->id}}/attr',
                    type: "post",
                    data: {'users': $('#users{{$evento->id}}').val(), 'id': {{$evento->id}}},
                    success: function (data) {
                        $el.faLoading(false);
                        console.log(data);
                        var sucesso = $('#sucesso').html();
                        $('#erro{{$evento->id}}').html(sucesso);
                        var $select = $('#users{{$evento->id}}').selectize();
                        var control = $select[0].selectize;
                        control.clear();
                        $('#cancel{{$evento->id}}_btn').click(function(){
                            $('.alert').alert('close');
                        });
                    },
                    error: function(data) {
                        $el.faLoading(false);

                        var erro2 = $('#erro2').html();
                        $('#erro{{$evento->id}}').html(erro2);
                        var $select = $('#users{{$evento->id}}').selectize();
                        var control = $select[0].selectize;
                        control.clear();
                        $('#cancel{{$evento->id}}_btn').click(function(){
                            $('.alert').alert('close');
                        });
                    },
                });
            }else{
                $el.faLoading(false);
                var erro = $('#erro').html();
                $('#erro{{$evento->id}}').html(erro);
                $('#cancel{{$evento->id}}_btn').click(function(){
                    $('.alert').alert('close');
                });
            }

        });
        $('#cancel{{$evento->id}}_btn').click(function(){
            var $select = $('#users{{$evento->id}}').selectize();
            var control = $select[0].selectize;
            control.clear();
        });


    });
</script>
























