<?php

namespace App\DataTables;

use App\Models\Stock;
use Form;
use Yajra\Datatables\Services\DataTable;
use DB;

class StockDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('actions', function ($data) {
                            return '
                            ' . Form::open(['route' => ['admin.equipamentos.stock.destroy', $data->id], 'method' => 'delete']) . '
                            <div class=\'btn-group\'>
                                <a href="' . route('admin.equipamentos.stock.show', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="' . route('admin.equipamentos.stock.edit', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-edit"></i></a>
                                ' . Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'onclick' => "return confirm('Are you sure?')"
                            ]) . '
                            </div>
                            ' . Form::close() . '
                            ';
                        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $stocks = Stock::query()
        ->select([
            'stock.id',
            'stock.equipamento_id',
            'stock.armazem_id',
            'stock.stock',
            'equipamentos.modelo as modelo',
            'armazens.nome as nome',
            'stock.created_at',
            'stock.updated_at'
        ])->leftjoin('equipamentos','stock.equipamento_id','=','equipamentos.id')
          ->leftjoin('armazens', 'stock.armazem_id','=','armazens.id');

        return $this->applyScopes($stocks);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns(),
                [
                    'actions' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => true,
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            //'equipamento_id' => ['name' => 'equipamento_id', 'data' => 'equipamento_id'],
            'equipamento' => ['name' => 'modelo', 'data' => 'modelo'],
           // 'armazem_id' => ['name' => 'armazem_id', 'data' => 'armazem_id'],
            'armazem' => ['name' => 'nome', 'data' => 'nome'],
            'stock' => ['name' => 'stock', 'data' => 'stock'],
            'created_at' => ['name' => 'created_at', 'data' => 'created_at'],
            'updated_at' => ['name' => 'updated_at', 'data' => 'updated_at']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'stocks';
    }
}
