<?php


namespace App\Http\Fruta;


class Banana
{
    public function __construct(

    )
    {
        $this->banana = 1;
    }

    public function descasca()
    {
        $this->banana->casca = 0;
    }

    public function come()
    {
        $this->banana = 0;
    }
}