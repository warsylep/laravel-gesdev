<?php

namespace App\Models\Evento;

use Illuminate\Database\Eloquent\Model;

class Lugar extends Model
{
    protected $table = 'lugares';
    protected $fillable =  ['nome', 'created_at', 'updated_at'];

    public function eventos(){
        return $this->hasMany('App\Models\Evento\Evento','lugar_id');
    }

}
