<?php

namespace App\Repositories;

use App\Models\TipoEquipamento;
use InfyOm\Generator\Common\BaseRepository;

class TipoEquipamentoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nome'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TipoEquipamento::class;
    }

    public function getAllTipos($order_by = 'id', $sort = 'asc'){
        return TipoEquipamento::orderBy($order_by, $sort)->get();
    }
}
