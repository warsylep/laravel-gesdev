<?php

namespace App\Listeners\Backend\Evento;

use App\Events\Backend\Evento\NovoEvento;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Emails\Evento\EventoEmail;

class NovoEventoListener
{
    /**
     * Create the event listener.
     *
     * @param EventoEmail $mailer
     */
    public function __construct(EventoEmail $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * Handle the event.
     *
     * @param  NovoEvento  $event
     * @return void
     */
    public function handle(NovoEvento $event)
    {
        $evento = $event->evento;
        $this->mailer->sendNewEventoEmailAdmins($evento);
        $this->mailer->sendNewEventoEmailOwner($evento);
    }
}
