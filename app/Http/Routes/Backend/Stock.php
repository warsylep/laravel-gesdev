<?php
/**
 * Created by PhpStorm.
 * User: nuno.silva
 * Date: 20-05-2016
 * Time: 15:44
 */

Route::group([
    'prefix'     => 'equipamentos',
    'namespace'  => 'Equipamento',
], function() {

    route::resource('stock','StockController');

});