<?php

namespace App\Events\Backend\Evento;

use App\Events\Event;
use App\Models\Evento\Evento;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventoAlterado extends Event
{
    use SerializesModels;
    public $evento;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Evento $evento)
    {
        $this->evento = $evento;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
