$(document).ready(function () {

    toggleFields1(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    toggleFields2();
    $('select[name=lugar_id]').change(function () {
        toggleFields1();
    });
    $('select[name=tipo_evento_id]').change(function(){
        toggleFields2();
    })





});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields1() {
    if ($('select[name=lugar_id]').val() == 7) {
        $('input[name=lugar_outro]').parent().parent().show();
        $('input[name=lugar_outro]').addClass('col-lg-offset-1');

    }else {
        $('input[name=lugar_outro]').val("");
        $('input[name=lugar_outro]').parent().parent().hide();
    }
}

function toggleFields2() {
    if ($('select[name=tipo_evento_id]').val() == 8) {
        $('input[name=tipo_evento_outro]').parent().parent().show();
        $('input[name=tipo_evento_outro]').addClass('col-lg-offset-1');
    } else {
        $('input[name=tipo_evento_outro]').val("");
        $('input[name=tipo_evento_outro]').parent().parent().hide();
    }
}/**
 * Created by nuno.silva on 25-01-2016.
 */
