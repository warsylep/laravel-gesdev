<?php
/**
 * Created by PhpStorm.
 * User: nuno.silva
 * Date: 17-01-2016
 * Time: 16:48
 */

Route::group([
    'prefix'     => 'eventos',
    'namespace'  => 'Evento',
    'middleware' => 'access.routeNeedsPermission:acesso-eventos',
], function() {

    Route::group([
        'middleware' => 'access.routeNeedsPermission:listar-eventos'],
        function() {
            route::get('/evento/poratribuir','EventoController@PorAtribuir')->name('admin.eventos.evento.poratribuir');
            route::get('/evento/atribuidos','EventoController@eventosAttr')->name('admin.eventos.evento.atribuidos');
            route::get('calendario','EventoController@calendario')->name('admin.eventos.calendario');
            route::resource('evento', 'EventoController', ['except' => 'show','create','store']);

    });
    route::group([
        'middleware' => 'access.routeNeedsPermission:criar-eventos'],
        function() {
            route::resource('evento','EventoController',[ 'only' => ['create','store']]);

            route::get('/evento/meus','EventoController@meusEventos')->name('admin.eventos.evento.meus');
            Route::resource('evento', 'EventoController', ['only' => ['show']]);
        }
    );

    Route::group(['prefix' => 'evento/{id}',
        'where' => ['id' => '[0-9]+'],
        'middleware' => 'access.routeNeedsPermission:criar-eventos'
    ], function() {
        Route::get('usersAttr','EventoController@usersAttr')->name('admin.eventos.evento.usersattr');

    });


    Route::group(['prefix' => 'evento/{id}',
        'where' => ['id' => '[0-9]+'],
        'middleware' => 'access.routeNeedsPermission:editar-eventos'
    ], function() {
        Route::get('autoattr','EventoController@autoAtribuir')->name('admin.eventos.evento.autoattr');
        Route::get('conf/{status}', 'EventoController@conf')->name('admin.eventos.evento.conf')->where(['status' => '[0,1]']);
        Route::get('attr','EventoController@attr')->name('admin.eventos.evento.attr');
        Route::post('attr', 'EventoController@postAttr')->name('admin.eventos.evento.postattr');

    });


});

Route::group([
    'prefix'     => 'eventos',
    'namespace'  => 'Evento',
    'middleware' => 'access.routeNeedsPermission:acesso-lugar',
], function() {
    Route::resource('local','LugarController');
});

Route::group([
    'prefix'     => 'eventos',
    'namespace'  => 'Evento',
    'middleware' => 'access.routeNeedsPermission:acesso-tipo',
], function() {
    Route::resource('tipo','TipoEventoController');
});

