@extends('backend.layouts.master')

@section('content')
    <section class="content-header">
        <h1>
            Tipo de eventos - Ver
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                  @include('backend.evento.tipo.show_fields')
                  <a href="{!! route('admin.eventos.tipo.index') !!}" class="btn btn-default">Voltar</a>
                </div>
            </div>
        </div>
    </div>
@endsection
