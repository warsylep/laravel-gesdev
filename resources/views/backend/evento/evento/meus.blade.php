@extends ('backend.layouts.master')

@section ('title', trans('labels.backend.eventos.evento.gestao'))

@section('page-header')
    <h1>
        {{ trans('labels.backend.eventos.evento.gestao') }}
        <small>{{ trans('labels.backend.eventos.evento.meus') }}</small>
    </h1>
@endsection

@section('content')
    <div class="box">


            <!-- Nav tabs -->
        <ul class="nav nav-pills" role="tablist">
            <li role="presentation" id="meus" class="active"><a href="meus" aria-controls="meus" ><h4><i class="fa fa-user"></i> Meus Eventos</h4></a></li>
            @roles([1,3,5])
            <li role="presentation" id="atribuidos"><a href="atribuidos" aria-controls="atribuidos" ><h4><i class="fa fa-arrow-circle-right"></i> Atribuidos a mim</h4></a></li>
            <li role="presentation" id="por_atribuir" ><a href="poratribuir" aria-controls="por_atribuir" ><h4><i class="fa fa-question-circle"></i> Por atribuir</h4></a></li>

            @endauth
        </ul>

            <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="meus">
                <div class="box-body">
                    @if(isset($eventos))
                        <div class="table-responsive">
                            <table class="text-center table table-striped table-bordered table-hover" id="dados">
                                <thead>
                                <tr>
                                    <th>{{ trans('labels.backend.eventos.evento.tabela.owner') }}</th>
                                    <th>{{ trans('labels.backend.eventos.evento.tabela.titulo') }}</th>
                                    <th>{{ trans('labels.backend.eventos.evento.tabela.data') }}</th>
                                    <th>{{ trans('labels.backend.eventos.evento.tabela.lugar') }}</th>
                                    <th>{{ trans('labels.backend.eventos.evento.tabela.tipo') }}</th>
                                    <th>{{ trans('labels.backend.eventos.evento.tabela.atribuido') }}</th>
                                    <th>{{ trans('labels.backend.eventos.evento.tabela.confirmado') }}</th>
                                    <th>{{ trans('labels.general.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($eventos as $evento)

                                    <tr>
                                        <td>{!! $evento->owner->name !!}</td>
                                        <td>{!! $evento->titulo !!}</td>
                                        <td>{!! $evento->dia !!}</td>
                                        <td>{!! $evento->lugar->nome !!}</td>
                                        <td>{!! $evento->tipo_evento->tipo_evento !!}</td>
                                        <td>{!! $evento->atribuido_label !!}</td>
                                        <td>{!! $evento->confirmado_label !!}</td>
                                        <td>{!! $evento->view_button !!}{!! $evento->edit_button !!}{!! $evento->delete_button !!} {!! $evento->conf_button !!} {!! $evento->attr_button !!} {!! $evento->auto_button !!}</td>
                                        @roles([1,3,5])
                                        @include('backend.evento.evento.attr')
                                        @endauth
                                        @role(4)
                                        @include('backend.evento.evento.attr_user')
                                        @endauth
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @else
                        <h2>Sem Eventos</h2>
                    @endif

                    <div class="pull-left">

                    </div>

                    <div class="pull-right">

                    </div>

                    <div class="clearfix"></div>
                </div><!-- /.box-body -->
            </div>
            @role('Administrator')

            @endauth

        </div>


    </div><!--box-->



{!! Html::script('js/backend/attr.js') !!}

@stop
