<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Armazem
 * @package App\Models
 */
class Armazem extends Model
{

    public $table = 'armazens';
    


    public $fillable = [
        'nome'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'nome' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id' => 'unique',
        'nome' => 'required|max:255|unique:armazens'
    ];
}
