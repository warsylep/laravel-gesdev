<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class EventoServiceProvider extends ServiceProvider

{
    public function boot()
    {

    }
    public function register()

    {

        $this->app->bind(
            'App\Repositories\Backend\Evento\EventoContract',
            'App\Repositories\Backend\Evento\EloquentEventoRepository'
        );
        $this->app->bind(
            'App\Repositories\Backend\Lugar\LugarContract',
            'App\Repositories\Backend\Lugar\EloquentLugarRepository'
        );
        $this->app->bind(
            'App\Repositories\Backend\TipoEvento\TipoEventoContract',
            'App\Repositories\Backend\TipoEvento\EloquentTipoEventoRepository'
        );
    }

}