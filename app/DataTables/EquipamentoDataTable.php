<?php

namespace App\DataTables;

use App\Models\Equipamento;
use Form;
use Yajra\Datatables\Services\DataTable;

class EquipamentoDataTable extends DataTable
{

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajax()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('actions', function ($data) {
                            return '
                            ' . Form::open(['route' => ['admin.equipamentos.equipamento.destroy', $data->id], 'method' => 'delete']) . '
                            <div class=\'btn-group\'>
                                <a href="' . route('admin.equipamentos.equipamento.show', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-eye-open"></i></a>
                                <a href="' . route('admin.equipamentos.equipamento.edit', [$data->id]) . '" class=\'btn btn-default btn-xs\'><i class="glyphicon glyphicon-edit"></i></a>
                                ' . Form::button('<i class="glyphicon glyphicon-trash"></i>', [
                                'type' => 'submit',
                                'class' => 'btn btn-danger btn-xs',
                                'onclick' => "return confirm('Are you sure?')"
                            ]) . '
                            </div>
                            ' . Form::close() . '
                            ';
                        })
            ->make(true);
    }

    /**
     * Get the query object to be processed by datatables.
     *
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        $equipamentos = Equipamento::query()
                    ->select([
                        'equipamentos.id',
                        'equipamentos.tipo_equipamento_id',
                        'equipamentos.modelo',
                        'equipamentos.obs',
                        'equipamentos.created_at',
                        'equipamentos.updated_at',
                        'tipo_equipamentos.nome as tipo'
                    ])->leftJoin('tipo_equipamentos', 'equipamentos.tipo_equipamento_id', '=', 'tipo_equipamentos.id');

        return $this->applyScopes($equipamentos);
    }




    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge(
                $this->getColumns(),
                [
                    'actions' => [
                        'orderable' => false,
                        'searchable' => false,
                        'printable' => false,
                        'exportable' => false
                    ]
                ]
            ))
            ->parameters([
                'dom' => 'Bfrtip',
                'scrollX' => true,
                'buttons' => [
                    'csv',
                    'excel',
                    'pdf',
                    'print',
                    'reset',
                    'reload'
                ],
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    private function getColumns()
    {
        return [
            'tipo' => ['name' => 'tipo', 'data' => 'tipo'],
            //'tipo_equipamento_id' => ['name' => 'tipo_equipamento_id', 'data' => 'tipo_equipamento_id'],
            'modelo' => ['name' => 'modelo', 'data' => 'modelo'],
            'obs' => ['name' => 'obs', 'data' => 'obs'],
            'created_at' => ['name' => 'created_at', 'data' => 'created_at'],
            'updated_at' => ['name' => 'updated_at', 'data' => 'updated_at']
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'equipamentos';
    }
}
