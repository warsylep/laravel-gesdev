<?php

namespace App\Repositories\Backend\Lugar;

use App\Models\Evento\Lugar;
use App\Exceptions\GeneralException;


class EloquentLugarRepository implements LugarContract
{
    /**
     * @var EventoContract
     */
    protected $lugar;


    /**
     *
     */
    public function __construct(

    )
    {

    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOrThrowException($id)
    {
        $lugar = Lugar::findOrFail($id);

        return $lugar;
    }


    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAllLugares($order_by = 'id', $sort = 'asc') {
        return Lugar::orderBy($order_by, $sort)
            ->get();
    }


    /**
     * @param $per_page
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getLugaresPaginated($per_page, $order_by = 'id', $sort = 'asc') {
        return Evento::orderBy($order_by, $sort)->paginate($per_page);
    }


    /**
     * @param $input
     * @return bool
     * @throws GeneralException
     */
    public function create($input)
    {
        $lugar = $this->createLugarStub($input);

        if ($lugar->save()) {

            //Send confirmation email if requested


            return true;
        }

        throw new GeneralException('Erro ao criar lugar');
    }


    /**
     * @param $id
     * @param $input
     * @return bool
     * @throws GeneralException
     */
    public function update($id, $input){
        $lugar = $this->findOrThrowException($id);
        if ($lugar->update($input)) {
            return true;
        }
        throw new GeneralException('Erro ao actualizar o lugar');
    }

    /**
     * @param $id
     * @throws GeneralException
     * @return bool
     */
    public function delete($id) {
        $lugar = $this->findOrThrowException($id);

        $lugar->delete();

    }

    /**
     * @param $input
     * @return Evento
     */
    function createLugarStub($input)
    {

        $lugar = new Lugar;
        $lugar->nome = $input['nome'];

        return $lugar;
    }
}