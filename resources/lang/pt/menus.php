<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title' => 'Gestão de utilizadores',

            'permissions' => [
                'all' => 'Todas as  Permissões',
                'create' => 'Criar Permissão',
                'edit' => 'Editar Permissão',
                'groups' => [
                    'all' => 'Todos os Grupos',
                    'create' => 'Criar Grupo',
                    'edit' => 'Editar Grupo',
                    'main' => 'Grupos',
                ],
                'main' => 'Permissões',
                'management' => 'Gestão de Permissões',
            ],

            'roles' => [
                'all' => 'Todos os perfis',
                'create' => 'Criar perfil',
                'edit' => 'Editar perfil',
                'management' => 'Gestão de perfis',
                'main' => 'perfis',
            ],

            'users' => [
                'all' => 'Todos os utilizadores',
                'change-password' => 'Alterar password',
                'create' => 'Criar utilizador',
                'deactivated' => 'Utilizadores desactivados',
                'deleted' => 'Utilizadores apagados',
                'edit' => 'Editar utilizador',
                'main' => 'utilizadores',
            ],
        ],

        'log-viewer' => [
            'main' => 'Ver Logs',
            'dashboard' => 'Dashboard',
            'logs' => 'Logs',
        ],

        'sidebar' => [
            'dashboard' => 'Dashboard',
            'general' => 'Geral',
        ],
        'eventos' => [
            'title' => 'Gestão de Eventos',
            'evento' => [
                'subtitle' => 'Eventos',
                'listar' => 'Listar eventos',
                'criar' => 'Criar evento',
                'meus' => 'Os meus eventos',
            ],
            'lugar' => [
                'subtitle' => 'Locais',
                'listar' => 'Listar locais',
                'criar' => 'Criar local',
            ],
            'tiposevento' => [
                'subtitle' => 'Tipos de evento',
                'listar' =>'Listar tipos de evento',
                'criar' => 'Criar tipo de evento',
            ],
        ],

        'equipamentos' => [
            'title' => 'Gestão de Equipamentos',
            'equipamentos' => 'Equipamentos',
            'listar' => 'Listar equipamentos',
            'criar' => 'Adicionar equipamento',
            'tipo' => [
              'title' => 'Tipos de equipamentos',
              'listar' => 'Listar tipos',
              'criar' => 'Criar tipo'
            ],
            'armazens' => [
              'title' => 'Armazéns',
              'listar' => 'Listar armazéns',
              'criar' => 'Criar armazém'
            ],
            'equipamento' => [
                'title' => 'Equipamentos',
                'listar' => 'Listar equipamentos',
                'criar' => 'Criar equipamento'
            ],
            'stock' => [
                'title' => 'Stock',
                'listar' => 'Listar stocks',
                'criar' => 'Adicionar stock'
            ]
        ],

    ],



    'language-picker' => [
        'language' => 'Idioma',
        /**
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
            'en' => 'English',
            'fr' => 'French',
            'it' => 'Italian',
            'pt' => 'Português',
            'sv' => 'Swedish',
        ],
    ],
];
