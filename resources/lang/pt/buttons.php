<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in buttons throughout the system.
    | Regardless where it is placed, a button can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'users' => [
                'activate' => 'Ativar',
                'change_password' => 'Alterar password',
                'deactivate' => 'Desativar',
                'delete_permanently' => 'Apagar Permanentemente',
                'resend_email' => 'Reenviar e-mail de confirmação',
                'restore_user' => 'Restaurar utilizador',
            ],
        ],
    ],

    'general' => [
        'cancel' => 'Cancelar',
        'voltar' => 'Voltar',
        'conf' => 'Confirmar',
        'unconf' => 'Desconfirmar',
        'attr' => 'Atribuir',

        'crud' => [
            'create' => 'Criar',
            'delete' => 'Apagar',
            'edit' => 'Editar',
            'update' => 'Atualizar',
            'auto' => 'Atribuir a mim',
        ],

        'save' => 'Guardar',
        'view' => 'Ver',
    ],
];
