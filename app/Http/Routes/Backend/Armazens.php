<?php


Route::group([
    'prefix'     => 'equipamentos',
    'namespace'  => 'Equipamento',
], function() {

    route::resource('armazens','ArmazemController');

});
