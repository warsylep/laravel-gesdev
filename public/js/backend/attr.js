/**
 * Created by nuno.silva on 12-02-2016.
 */

$(document).ready(function(){

    $('body').on('click', function (e) {
        //only buttons
        if ($(e.target).data('toggle') !== 'popover'
            && $(e.target).parents('.popover.in').length === 0) {
            $('[data-original-title]').popover('hide');
        }
        //buttons and icons within buttons
        /*
         if ($(e.target).data('toggle') !== 'popover'
         && $(e.target).parents('[data-toggle="popover"]').length === 0
         && $(e.target).parents('.popover.in').length === 0) {
         $('[data-toggle="popover"]').popover('hide');
         }
         */
    });
        $('#dados').dataTable({
            "language": {
                "url": "http://10.0.99.220/js/backend/pt.json"
            },
            responsive: true,
            ordering: false,
            "pageLength": 8
        });
});