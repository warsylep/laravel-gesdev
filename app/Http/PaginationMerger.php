<?php
/**
 * Created by PhpStorm.
 * User: nuno.silva
 * Date: 26-02-2016
 * Time: 11:12
 */

namespace App\Http;
use Illuminate\Pagination\LengthAwarePaginator;


/**
 * Class PaginationMerger
 * @package App\Http
 */
class PaginationMerger
{

    /**
     * @param LengthAwarePaginator $collection1
     * @param LengthAwarePaginator $collection2
     * @return LengthAwarePaginator
     */
    static public function merge(LengthAwarePaginator $collection1, LengthAwarePaginator $collection2)
    {
        $total = $collection1->total() + $collection2->total();

        $perPage = ($collection1->perPage() + $collection2->perPage()) / 2;

        $items = array_merge($collection1->items(), $collection2->items());

        $paginator = new LengthAwarePaginator($items, $total, $perPage);

        return $paginator;
    }

}