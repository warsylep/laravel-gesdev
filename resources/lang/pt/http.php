<?php

return [

    /*
    |--------------------------------------------------------------------------
    | HTTP Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in the views/errors files.
    |
    */

    '404' => [
        'title' => 'Página não encontrada',
        'description' => 'Desculpe, mas a página que está a tentar aceder não existe.',
    ],

    '503' => [
        'title' => 'Voltamos em breve',
        'description' => 'Voltamos em breve.',
    ],

];