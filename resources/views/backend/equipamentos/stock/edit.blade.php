@extends('backend.layouts.master')

@section('content')
   <section class="content-header">
           <h1>
               Stock - Alterar
           </h1>
   </section>
   <div class="content">

       <div class="box box-primary">

           <div class="box-body">
               <div class="row">
                   {!! Form::model($stock, ['route' => ['admin.equipamentos.stock.update', $stock->id], 'method' => 'patch']) !!}

                    @include('backend.equipamentos.stock.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection