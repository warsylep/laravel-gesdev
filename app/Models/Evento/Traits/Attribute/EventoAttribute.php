<?php


namespace App\Models\Evento\Traits\Attribute;

/**
 * Created by PhpStorm.
 * User: nuno.silva
 * Date: 23-01-2016
 * Time: 20:13
 */
trait  EventoAttribute
{
    /**
     * @return bool
     */
    public function isConfirmado()
    {
        return $this->confirmado == 1;


    }

    public function isAtribuido()
    {
        return $this->atribuido == 1;
    }



    /**
     * @return string
     */
    public function getConfirmadoLabelAttribute()
    {
        if ($this->isConfirmado())
            return "<label class='label label-success' >Sim</label>";
        return "<label class='label label-danger'>Não</label>";
    }


    public function getAtribuidoLabelAttribute()
    {
        if ($this->isAtribuido())
            return "<label class='label label-success btn' id='la_".$this->id."' >Sim</label>";
        return "<label class='label label-danger'>Não</label>";
    }


    public function getViewButtonAttribute()
    {
        if (access()->allow('ver-eventos')) {
            return '

                    <a href="' . route('admin.eventos.evento.show', $this->id) . '" class="btn btn-xs btn-success"><i class="fa fa-eye" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.view') . '"></i></a>

                    ';
        }
    }


    public function getAttrButtonAttribute()
    {
        if (access()->allow('atribuir-evento')){
            return '
                    <span data-toggle="modal" data-target="#attr'.$this->id .'">
                    <a class="btn btn-xs bg-maroon"><i class="fa fa-users" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.attr') . '"></i></a>
                    </span>
                    ';
        }
    }




    public function getConfButtonAttribute()
    {
        switch ($this->confirmado) {
            case 0:
                if (access()->allow('confirmar-eventos')) {
                    return '<a href="' . route('admin.eventos.evento.conf', [$this->id, 1]) . '" class="btn btn-xs btn-info"><i class="fa fa-check-square" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.conf') . '"></i></a> ';
                }

                break;

            case 1:
                if (access()->allow('confirmar-eventos')) {
                    return '<a href="' . route('admin.eventos.evento.conf', [$this->id, 0]) . '" class="btn btn-xs btn-warning"><i class="fa fa-check-square-o" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.unconf') . '"></i></a> ';
                }

                break;

            default:
                return '';
            // No break
        }

        return '';

    }



    /**
     * @return string
     */
    public function getEditButtonAttribute()
    {
        if (access()->allow('editar-eventos')) {
            return '<a href="' . route('admin.eventos.evento.edit', $this->id) . '" class="btn btn-xs btn-primary"><i class="fa fa-pencil" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.edit') . '"></i></a> ';
        }

        return '';
    }

    public function getDeleteButtonAttribute()
    {
        if (access()->allow('apagar-eventos')) {
            return '<a href="' . route('admin.eventos.evento.destroy', $this->id) . '" data-method="delete" class="btn btn-xs btn-danger"><i class="fa fa-trash" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.delete') . '"></i></a> ';
        }

        return '';
    }

    public function getAutoButtonAttribute()
    {
        if (access()->allow('auto-atribuir')) {
            if(!$this->isAtribuido()) {
                return '<a href="' . route('admin.eventos.evento.autoattr', $this->id) . '" class="btn btn-xs bg-teal"><i class="fa fa-user" data-toggle="tooltip" data-placement="top" title="' . trans('buttons.general.crud.auto') . '"></i></a> ';
            }
        }


        return '';
    }

}



