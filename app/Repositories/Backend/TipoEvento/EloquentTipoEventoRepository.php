<?php

namespace App\Repositories\Backend\TipoEvento;

use App\Models\Evento\TipoEvento;
use App\Exceptions\GeneralException;


class EloquentTipoEventoRepository implements TipoEventoContract
{

    protected $tipo;


    /**
     *
     */
    public function __construct(

    )
    {

    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOrThrowException($id)
    {
        $tipo = TipoEvento::findOrFail($id);

        return $tipo;
    }


    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAllTipoEvento($order_by = 'id', $sort = 'asc') {
        return TipoEvento::orderBy($order_by, $sort)
            ->get();
    }


    /**
     * @param $per_page
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getTipoEventoPaginated($per_page, $order_by = 'id', $sort = 'asc') {
        return TipoEvento::orderBy($order_by, $sort)->paginate($per_page);
    }


    /**
     * @param $input
     * @return bool
     * @throws GeneralException
     */
    public function create($input)
    {
        $tipo = $this->createTipoEventoStub($input);

        if ($tipo->save()) {

            //Send confirmation email if requested


            return true;
        }

        throw new GeneralException('Erro ao criar tipo de evento');
    }


    /**
     * @param $id
     * @param $input
     * @return bool
     * @throws GeneralException
     */
    public function update($id, $input){
        $tipo = $this->findOrThrowException($id);
        if ($tipo->update($input)) {
            return true;
        }
        throw new GeneralException('Erro ao actualizar o tipo de evento');
    }

    /**
     * @param $id
     * @throws GeneralException
     * @return bool
     */
    public function delete($id) {
        $tipo = $this->findOrThrowException($id);

        $tipo->delete();

    }

    /**
     * @param $input
     * @return Evento
     */
    function createTipoEventoStub($input)
    {

        $tipo = new TipoEvento;
        $tipo->tipo_evento = $input['tipo_evento'];

        return $tipo;
    }
}