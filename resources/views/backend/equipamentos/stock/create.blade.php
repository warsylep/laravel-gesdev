@extends('backend.layouts.master')
<?php


?>
@section('content')
 <section class="content-header">
        <h1>
            Stock - Adicionar
        </h1>
    </section>
    <div class="content">

        <div class="box box-primary">

            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'admin.equipamentos.stock.store']) !!}

                              @include('backend.equipamentos.stock.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
