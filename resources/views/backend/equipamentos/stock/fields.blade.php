<!-- Equipamento Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipamento_id', 'Equipamento Id:') !!}
    {!! Form::select('equipamento_id',$equip, null, ['class' => 'form-control']) !!}
</div>

<!-- Armazem Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('armazem_id', 'Armazem Id:') !!}
    {!! Form::select('armazem_id',$armaz, null, ['class' => 'form-control']) !!}
</div>

<!-- Stock Field -->
<div class="form-group col-sm-6">
    {!! Form::label('stock', 'Stock:') !!}
    {!! Form::text('stock', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('admin.equipamentos.stock.index') !!}" class="btn btn-default">Voltar</a>
</div>
