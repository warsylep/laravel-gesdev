<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{!! access()->user()->picture !!}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>{!! access()->user()->name !!}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('strings.backend.general.search_placeholder') }}"/>
                  <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                  </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            <!-- Optionally, you can add icons to the links -->
            <li class="{{ Active::pattern('admin/dashboard') }}">
                <a href="{!! route('admin.dashboard') !!}"><i class="fa fa-tachometer"></i><span>{{ trans('menus.backend.sidebar.dashboard') }}</span></a>
            </li>

            @permission('view-access-management')
                <li class="{{ Active::pattern('admin/access/*') }}">
                    <a href="{!!url('admin/access/users')!!}"><i class="fa fa-users"></i><span>{{ trans('menus.backend.access.title') }}</span></a>
                </li>
            @endauth

            @permission('acesso-eventos')
                <li class="{{ Active::pattern('admin/eventos/*') }} treeview">
                    <a href="#">
                        <i class="fa fa-tasks"></i>
                        <span>{{ trans('menus.backend.eventos.title') }}</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu {{ Active::pattern('admin/eventos/*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/eventos/*', 'display: block;') }}">
                        <li class="{{ Active::pattern('admin/eventos/evento*') }}">
                            <a href="#">
                                <i class="fa fa-circle-o"></i>
                                <span>{{ trans('menus.backend.eventos.evento.subtitle') }}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu {{ Active::pattern('admin/eventos/evento*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/eventos/evento*', 'display: block;') }}">
                               @permission('meus-eventos')
                                <li class="{{Active::pattern('admin/eventos/evento/meus')}}">
                                    <a href="{!! url('admin/eventos/evento/meus') !!}">{{ trans('menus.backend.eventos.evento.meus') }}</a>
                                </li>
                                @endauth
                                @permission('listar-eventos')
                                <li class="{{Active::pattern('admin/eventos/evento')}}">
                                    <a href="{!! url('admin/eventos/evento') !!}">{{ trans('menus.backend.eventos.evento.listar') }}</a>
                                </li>
                                @endauth
                                @permission('criar-eventos')
                                <li class="{{ Active::pattern('admin/eventos/evento/create') }}">
                                    <a href="{!! url('admin/eventos/evento/create') !!}">{{ trans('menus.backend.eventos.evento.criar') }}</a>
                                </li>
                                @endauth
                            </ul>

                        </li>
                        @permission('acesso-lugar')
                        <li class="{{ Active::pattern('admin/eventos/local*') }}">
                            <a href="#">
                                <i class="fa fa-circle-o"></i>
                                <span>{{ trans('menus.backend.eventos.lugar.subtitle') }}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu {{ Active::pattern('admin/eventos/local*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/eventos/local*', 'display: block;') }}">

                                <li class="{{Active::pattern('admin/eventos/local')}}">
                                    <a href="{!! url('admin/eventos/local') !!}">{{ trans('menus.backend.eventos.lugar.listar') }}</a>
                                </li>


                                <li class="{{Active::pattern('admin/eventos/local/create')}}">
                                    <a href="{!! url('admin/eventos/local/create') !!}">{{ trans('menus.backend.eventos.lugar.criar') }}</a>
                                </li>
                            </ul>
                        </li>
                        @endauth


                        @permission('acesso-tipo')
                        <li class="{{ Active::pattern('admin/eventos/tipo*') }}">
                            <a href="#">
                                <i class="fa fa-circle-o"></i>
                                <span>{{ trans('menus.backend.eventos.tiposevento.subtitle') }}</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu {{ Active::pattern('admin/eventos/tipo*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/eventos/tipo*', 'display: block;') }}">

                                <li class="{{Active::pattern('admin/eventos/tipo')}}">
                                    <a href="{!! url('admin/eventos/tipo') !!}">{{ trans('menus.backend.eventos.tiposevento.listar') }}</a>
                                </li>


                                <li class="{{Active::pattern('admin/eventos/tipo/create')}}">
                                    <a href="{!! url('admin/eventos/tipo/create') !!}">{{ trans('menus.backend.eventos.tiposevento.criar') }}</a>
                                </li>
                            </ul>
                        </li>
                        @endauth


                    </ul>
                </li>
            @endauth
            <!-- equipamento menu -->

            @permission('acesso-equipamentos')
            <li class="{{ Active::pattern('admin/equipamentos*') }} treeview">
                <a href="#">
                    <i class="fa fa-cube"></i>
                    <span>{{ trans('menus.backend.equipamentos.title') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/equipamentos*','menu-open') }}" style="display: none; {{ Active::pattern('admin/equipamentos*', 'display: block') }}">
                    <li class="{{ Active::pattern('admin/equipamentos/tipo*') }}">
                        <a href="#">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.equipamentos.tipo.title') }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ Active::pattern('admin/equipamentos/tipo*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/equipamentos/tipo*', 'display: block;') }}">

                            <li class="{{Active::pattern('admin/equipamentos/tipo')}}">
                                <a href="{!! url('admin/equipamentos/tipo') !!}">{{ trans('menus.backend.equipamentos.tipo.listar') }}</a>
                            </li>


                            <li class="{{Active::pattern('admin/equipamentos/tipo/create')}}">
                                <a href="{!! url('admin/equipamentos/tipo/create') !!}">{{ trans('menus.backend.equipamentos.tipo.criar') }}</a>
                            </li>
                        </ul>
                    </li>
                    <li class="{{Active::pattern('admin/equipamentos/armazens*')}}">
                        <a href="#">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.equipamentos.armazens.title') }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ Active::pattern('admin/equipamentos/armazens*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/equipamentos/armazens*', 'display: block') }}">
                            <li class="{{ Active::pattern('admin/equipamentos/armazens') }}">
                                <a href="{!! url('admin/equipamentos/armazens') !!}">{{ trans('menus.backend.equipamentos.armazens.listar') }}</a>
                            </li>
                            <li class="{{ Active::pattern('admin/equipamentos/armazens/create') }}">
                                <a href="{!! url('admin/equipamentos/armazens/create') !!}">{{ trans('menus.backend.equipamentos.armazens.criar') }}</a>
                            </li>
                        </ul>
                    </li>

                    <li class="{{Active::pattern('admin/equipamentos/equipamento*')}}">
                        <a href="#">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.equipamentos.equipamento.title') }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ Active::pattern('admin/equipamentos/equipamento*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/equipamentos/equipamento*', 'display: block') }}">
                            <li class="{{ Active::pattern('admin/equipamentos/equipamento') }}">
                                <a href="{!! url('admin/equipamentos/equipamento') !!}">{{ trans('menus.backend.equipamentos.equipamento.listar') }}</a>
                            </li>
                            <li class="{{ Active::pattern('admin/equipamentos/equipamento/create') }}">
                                <a href="{!! url('admin/equipamentos/equipamento/create') !!}">{{ trans('menus.backend.equipamentos.equipamento.criar') }}</a>
                            </li>
                        </ul>
                    </li>

                    <li class="{{Active::pattern('admin/equipamentos/stock*')}}">
                        <a href="#">
                            <i class="fa fa-circle-o"></i>
                            <span>{{ trans('menus.backend.equipamentos.stock.title') }}</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu {{ Active::pattern('admin/equipamentos/stock*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/equipamentos/stock*', 'display: block') }}">
                            <li class="{{ Active::pattern('admin/equipamentos/stock') }}">
                                <a href="{!! url('admin/equipamentos/stock') !!}">{{ trans('menus.backend.equipamentos.stock.listar') }}</a>
                            </li>
                            <li class="{{ Active::pattern('admin/equipamentos/stock/create') }}">
                                <a href="{!! url('admin/equipamentos/stock/create') !!}">{{ trans('menus.backend.equipamentos.stock.criar') }}</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </li>
            @endauth



            @role('1')
            <li class="{{ Active::pattern('admin/log-viewer*') }} treeview">
                <a href="#">
                    <i class="fa fa-file-text-o "></i>
                    <span>{{ trans('menus.backend.log-viewer.main') }}</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu {{ Active::pattern('admin/log-viewer*', 'menu-open') }}" style="display: none; {{ Active::pattern('admin/log-viewer*', 'display: block;') }}">
                    <li class="{{ Active::pattern('admin/log-viewer') }}">
                        <a href="{!! url('admin/log-viewer') !!}">{{ trans('menus.backend.log-viewer.dashboard') }}</a>
                    </li>
                    <li class="{{ Active::pattern('admin/log-viewer/logs') }}">
                        <a href="{!! url('admin/log-viewer/logs') !!}">{{ trans('menus.backend.log-viewer.logs') }}</a>
                    </li>
                </ul>
            </li>
            @endauth

        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>