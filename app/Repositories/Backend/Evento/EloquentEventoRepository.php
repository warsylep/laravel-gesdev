<?php

namespace App\Repositories\Backend\Evento;

use App\Models\Evento\Evento;
use App\Exceptions\GeneralException;
use App\Http\Emails\Evento\EventoEmail;
use App\Models\Access\User\User;
use App\Events\Backend\Evento\NovoEvento;
use Event;
use Carbon\Carbon;



class EloquentEventoRepository implements EventoContract
{
    /**
     * @var EventoContract
     */
    protected $evento;


    /**
     *
     */
    public function __construct(
            EventoEmail $eventoEmail,
            User $user
    )
    {
            $this->mailer = $eventoEmail;
            $this->user = $user;
        Carbon::setLocale('pt');
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findOrThrowException($id)
    {
        $evento = Evento::findOrFail($id);

        return $evento;
    }


    /**
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getAllEventos($order_by = 'dia', $sort = 'asc') {
        return Evento::orderBy($order_by, $sort)
            ->get();
    }


    public function getMeusEventos()
    {
      return User::find(access()->id())->owns()->orderBy('dia','asc')->get();
    }


    public function getEventosAtribuidos()
    {
        return User::find(access()->id())->eventos()->orderby('dia','asc')->paginate(8);
    }

    public function getUsersAttr($id)
    {

        return Evento::find($id)->user()->get();
    }

    public function getEventosPorAtribuir()
    {
        return Evento::where('atribuido',0)->paginate(8);
    }

    /**
     * @param $per_page
     * @param string $order_by
     * @param string $sort
     * @return mixed
     */
    public function getEventosPaginated($per_page, $order_by = 'dia', $sort = 'asc') {
        return Evento::orderBy($order_by, $sort)->paginate($per_page);
    }




    /**
     * @param $input
     * @return bool
     * @throws GeneralException
     */
    public function create($input)
    {
        $evento = $this->createEventoStub($input);

        if ($evento->save()) {
           event::fire(new NovoEvento($evento));
            return true;
        }

        throw new GeneralException('Erro ao criar evento');
    }


    /**
     * @param $id
     * @param $input
     * @return bool
     * @throws GeneralException
     */
    public function update($request, $id){
        $evento = $this->findOrThrowException($id);

        if ($evento->update($request)) {
            return true;

        }
        throw new GeneralException('Erro ao actualizar o evento');
    }

    /**
     * @param $id
     * @throws GeneralException
     * @return bool
     */
    public function delete($id) {
        $evento = $this->findOrThrowException($id, true);

            $evento->delete();

    }

    /**
     * @param $input
     * @return Evento
     */
    function  createEventoStub($input)
    {

        $evento = new Evento;
        $evento->owner_id = Access()->id();
        $evento->titulo = $input['titulo'];
        $evento->dia = $input['dia'];
        $evento->hora_inicio = $input['hora_inicio'];
        $evento->hora_fim = $input['hora_fim'];
        $evento->lugar_id = $input['lugar_id'];
        $evento->lugar_outro = $input['lugar_outro'];
        $evento->tipo_evento_id = $input['tipo_evento_id'];
        $evento->tipo_evento_outro = $input['tipo_evento_outro'];
        $cenas = $input['tipo_necessidade'];
        if(!$cenas){
            $c = '';
        }else {
            $c = implode(':', $cenas);
        }
        $evento->tipo_necessidade = $c;
        $evento->necessidade_extra = $input['necessidade_extra'];
        $evento->atribuido = 0;
        $evento->confirmado = 0;
        return $evento;
    }



    public function conf($id, $status)
    {
        $evento         = $this->findOrThrowException($id);
        $evento->confirmado = $status;

        if ($evento->save()) {
            return true;
        }

        throw new GeneralException(trans('exceptions.backend.eventos.evento.conf.error'));
    }

 public function atribuir($id){
     $evento = $this->findOrThrowException($id);
     $evento->atribuido = 1;
     if($evento->save()){
        return true;
     }
     throw new GeneralException(trans('exceptions.backend.eventos.evento.attr.error'));
 }
}