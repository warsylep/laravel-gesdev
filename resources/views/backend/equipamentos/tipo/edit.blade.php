@extends('backend.layouts.master')

@section('content')
   <section class="content-header">
           <h1>
               Tipos de Equipamento - Editar
           </h1>
   </section>
   <div class="content">

       <div class="box box-primary">

           <div class="box-body">
               <div class="row">
                   {!! Form::model($tipoEquipamento, ['route' => ['admin.equipamentos.tipo.update', $tipoEquipamento->id], 'method' => 'patch']) !!}

                    @include('backend.equipamentos.tipo.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection