<?php

namespace App\Http\Controllers\Backend\Evento;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\TipoEvento\TipoEventoContract;
use App\DataTables\TipoEventoDataTable;
use Illuminate\Http\Request;
Use Flash;

class TipoEventoController extends Controller
{
    public function __construct(TipoEventoContract $tipo)
    {
        $this->tipo = $tipo;
    }

    public function index(TipoEventoDataTable $tipoDataTable)
    {
        return $tipoDataTable->render('backend.evento.tipo.index');
    }

    public function create()
    {
        return view('backend.evento.tipo.create');
    }

    public function store(Request $request)
    {
        $input = $request->all();

        $tipo = $this->tipo->create($input);

        Flash::success('Tipo de evento guardado com sucesso.');

        return redirect(route('admin.eventos.tipo.index'));
    }

    public function show($id)
    {
        $tipo = $this->tipo->findOrThrowException($id);

        if (empty($tipo)) {
            Flash::error('Tipo de evento não encontrado');

            return redirect(route('admin.eventos.tipo.index'));
        }

        return view('backend.evento.tipo.show')->with('tipo', $tipo);
    }

    public function edit($id)
    {
        $tipo = $this->tipo->findOrThrowException($id);

        if (empty($tipo)) {
            Flash::error('Tipo de evento não encontrado');

            return redirect(route('admin.eventos.tipo.index'));
        }

        return view('backend.evento.tipo.edit')->with('tipo', $tipo);
    }

    public function update($id, Request $request)
    {
        $tipo = $this->tipo->findOrThrowException($id);

        if (empty($tipo)) {
            Flash::error('Tipo de evento não encontrado');

            return redirect(route('admin.eventos.tipo.index'));
        }

        $tipo = $this->tipo->update($id, $request->all());

        Flash::success('Tipo de evento actualizado com sucesso.');

        return redirect(route('admin.eventos.tipo.index'));
    }

    public function destroy($id)
    {
        $tipo = $this->tipo->findOrThrowException($id);

        if (empty($tipo)) {
            Flash::error('Tipo de evento não encontrado');

            return redirect(route('admin.eventos.tipo.index'));
        }

        $this->tipo->delete($id);

        Flash::success('Tipo de evento apagado com sucesso.');

        return redirect(route('admin.eventos.tipo.index'));
    }

}
