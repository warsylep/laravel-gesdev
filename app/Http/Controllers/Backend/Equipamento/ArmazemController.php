<?php

namespace App\Http\Controllers\Backend\Equipamento;

use App\DataTables\ArmazemDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateArmazemRequest;
use App\Http\Requests\UpdateArmazemRequest;
use App\Repositories\ArmazemRepository;
use Flash;
use InfyOm\Generator\Controller\AppBaseController;
use Response;

class ArmazemController extends AppBaseController
{
    /** @var  ArmazemRepository */
    private $armazemRepository;

    public function __construct(ArmazemRepository $armazemRepo)
    {
        $this->armazemRepository = $armazemRepo;
    }

    /**
     * Display a listing of the Armazem.
     *
     * @param ArmazemDataTable $armazemDataTable
     * @return Response
     */
    public function index(ArmazemDataTable $armazemDataTable)
    {
        return $armazemDataTable->render('backend.equipamentos.armazens.index');
    }

    /**
     * Show the form for creating a new Armazem.
     *
     * @return Response
     */
    public function create()
    {
        return view('backend.equipamentos.armazens.create');
    }

    /**
     * Store a newly created Armazem in storage.
     *
     * @param CreateArmazemRequest $request
     *
     * @return Response
     */
    public function store(CreateArmazemRequest $request)
    {
        $input = $request->all();

        $armazem = $this->armazemRepository->create($input);

        Flash::success('Armazém guardado com sucesso.');

        return redirect(route('admin.equipamentos.armazens.index'));
    }

    /**
     * Display the specified Armazem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $armazem = $this->armazemRepository->findWithoutFail($id);

        if (empty($armazem)) {
            Flash::error('Armazém não encontrado');

            return redirect(route('admin.equipamentos.armazens.index'));
        }

        return view('backend.equipamentos.armazens.show')->with('armazem', $armazem);
    }

    /**
     * Show the form for editing the specified Armazem.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $armazem = $this->armazemRepository->findWithoutFail($id);

        if (empty($armazem)) {
            Flash::error('Armazém não encontrado.');

            return redirect(route('admin.equipamentos.armazens.index'));
        }

        return view('backend.equipamentos.armazens.edit')->with('armazem', $armazem);
    }

    /**
     * Update the specified Armazem in storage.
     *
     * @param  int              $id
     * @param UpdateArmazemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateArmazemRequest $request)
    {
        $armazem = $this->armazemRepository->findWithoutFail($id);

        if (empty($armazem)) {
            Flash::error('Armazém não encontrado.');

            return redirect(route('admin.equipamentos.armazens.index'));
        }

        $armazem = $this->armazemRepository->update($request->all(), $id);

        Flash::success('Armazém actualizado com sucesso.');

        return redirect(route('admin.equipamentos.armazens.index'));
    }

    /**
     * Remove the specified Armazem from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $armazem = $this->armazemRepository->findWithoutFail($id);

        if (empty($armazem)) {
            Flash::error('Armazém não encontrado');

            return redirect(route('admin.equipamentos.armazens.index'));
        }

        $this->armazemRepository->delete($id);

        Flash::success('Armazém apagado com sucesso.');

        return redirect(route('admin.equipamentos.armazens.index'));
    }
}
